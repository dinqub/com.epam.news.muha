package com.epam.muha.dao;

import static org.junit.Assert.assertEquals;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.unitils.UnitilsJUnit4;

import com.epam.muha.dao.impl.NewsDAO;
import com.epam.muha.dao.impl.TagDAO;
import com.epam.muha.entity.News;
import com.epam.muha.entity.Tag;
import com.epam.muha.exception.DAOException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:springpropertiestest.xml" })
public class TagDAOTest extends UnitilsJUnit4 {

	//@Autowired
	//@Qualifier("TagDAO")
	private static TagDAO tagDAO;
	//@Autowired
	//@Qualifier("NewsDAO")
	private static NewsDAO newsDAO;
	
	public static java.util.Date today = new java.util.Date();
	public static Timestamp date=new Timestamp(today.getTime());

	private static Tag tag1;
	private static Tag tag2;
	private static Tag tag3;
	private static Tag tag4;
	private static Tag tag5;
	
	private static News news1;
	private static News news2;

	@SuppressWarnings("resource")
	@BeforeClass
	public static void setUpDao() throws DAOException {
		ClassPathXmlApplicationContext cx = new ClassPathXmlApplicationContext("springpropertiestest.xml");
		Locale.setDefault(Locale.ENGLISH);
		tagDAO=(TagDAO) cx.getBean("tagDAO");
		newsDAO=(NewsDAO) cx.getBean("newsDAO");
		tag1 = new Tag(1L,"Test1");
		tag2 = new Tag(2L,"Test2");
		tag3 = new Tag(3L,"Test3");
		tag4 = new Tag(4L,"Test4");
		tag5 = new Tag(5L,"Test5");
				
		Date dates=new Date(today.getTime());
		news1=new News(1L,"Test1","Test1","Test1",date,dates);
		news2=new News(2L,"Test2","Test2","Test2",date,dates);
		
		UtilitTest.createSequence(tagDAO);
		tagDAO.add(tag1);
		tagDAO.add(tag2);
		tagDAO.add(tag3);
		tagDAO.add(tag4);
		
		UtilitTest.createSequence(newsDAO);
		newsDAO.add(news1);
		newsDAO.add(news2);
		
		tagDAO.add(tag1.getId(),1L);
		tagDAO.add(tag2.getId(),2L);
	}

	@AfterClass
	public static void cleanDAO() throws DAOException {
		UtilitTest.deleteSequence(tagDAO);
		tagDAO.remove(1L);
		tagDAO.remove(2L);
		tagDAO.remove(3L);
		tagDAO.remove(5L);
		
		UtilitTest.deleteSequence(newsDAO);
		newsDAO.remove(1L);
		newsDAO.remove(2L);
	}
	
	@Test
	public void testAdd() throws DAOException {
		tagDAO.add(tag5);
		List<Tag> result = tagDAO.getAll();
		assertEquals(true, result.contains(tag5));
	}
	
	@Test
	public void testAddATagInNews() throws DAOException {
		tagDAO.add(tag2.getId(),1L);
		List<Tag> result = tagDAO.getTagsByNews(1L);		
		assertEquals(true, result.contains(tag2));
	}
	
	@Test
	public void testRemove() throws DAOException {
		tagDAO.remove(4L);
		List<Tag> result = tagDAO.getAll();
		assertEquals(false, result.contains(tag4));
	}
	
	@Test
	public void testRemoveTagFromNews() throws DAOException {
		tagDAO.remove(2L, tag2.getId());
		List<Tag> result = tagDAO.getTagsByNews(2L);
		assertEquals(false, result.contains(tag2));
	}
	
	@Test
	public void testUpdateTag() throws DAOException {
		tag3.setName("Test31");
		tagDAO.update(tag3);
		Tag result = tagDAO.get(3L);	
		assertEquals(tag3, result);
	}
	@Test
	public void testUpdateAllTagByNews() throws DAOException {
		List<Tag> tags=Arrays.asList(tag1,tag2,tag3);
		tagDAO.updateAllTagByNews(2L, tags);
		List<Tag> result = tagDAO.getTagsByNews(2L);		
		assertEquals(true, result.contains(tag1));		
		assertEquals(true, result.contains(tag2));	
		assertEquals(true, result.contains(tag3));
	}
	
	@Test
	public void testsGetTagsByNews() throws DAOException {
		List<Tag> result = tagDAO.getTagsByNews(1L);		
		assertEquals(true, result.contains(tag1));
	}
	
	@Test
	public void testGet() throws DAOException {
		Tag result = tagDAO.get(1L);
		assertEquals(tag1, result);
	}

	@Test
	public void testGetAll() throws DAOException {
		List<Tag> result = tagDAO.getAll();		
		assertEquals(true, result.contains(tag1));
		assertEquals(true, result.contains(tag2));
		assertEquals(true, result.contains(tag3));
	}

}
