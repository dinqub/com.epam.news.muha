package com.epam.muha.dao;

import static org.junit.Assert.assertEquals;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;
import java.util.Locale;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.unitils.UnitilsJUnit4;

import com.epam.muha.dao.IAuthorDAO;
import com.epam.muha.dao.INewsDAO;
import com.epam.muha.dao.impl.AuthorDAO;
import com.epam.muha.dao.impl.NewsDAO;
import com.epam.muha.entity.Author;
import com.epam.muha.entity.News;
import com.epam.muha.exception.DAOException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:springpropertiestest.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,DirtiesContextTestExecutionListener.class })
public class AuthorDAOTest extends UnitilsJUnit4 {

	//@Autowired( required )
	//@Qualifier("AuthorDAO")
	private static IAuthorDAO authorDAO;
//	@Autowired
//	@Qualifier("NewsDAO")
	private static INewsDAO newsDAO;
	
	public static java.util.Date today = new java.util.Date();
	public static Timestamp date=new Timestamp(today.getTime());

	private static Author author1;
	private static Author author2;
	private static Author author3;
	private static Author author4;
	
	private static News news1;
	private static News news2;

	@BeforeClass
	public static void setUpDao() throws DAOException {
		@SuppressWarnings("resource")
		ClassPathXmlApplicationContext cx = new ClassPathXmlApplicationContext("springpropertiestest.xml");
		Locale.setDefault(Locale.ENGLISH);
		authorDAO=(AuthorDAO) cx.getBean("authorDAO");
		newsDAO=(NewsDAO) cx.getBean("newsDAO");
		author1 = new Author(1L,"Test1");
		author2 = new Author(2L,"Test2");
		author3 = new Author(3L,"Test3");
		author4 = new Author(4L,"Test4");
		
		Date dates=new Date(today.getTime());
		news1=new News(1L,"Test1","Test1","Test1",date,dates);
		news2=new News(2L,"Test2","Test2","Test2",date,dates);
		
		
		UtilitTest.createSequence(authorDAO);
		authorDAO.add(author1);
		authorDAO.add(author2);
		authorDAO.add(author3);

		UtilitTest.createSequence(newsDAO);
		newsDAO.add(news1);
		newsDAO.add(news2);
		
		authorDAO.add(author2,2L);
	}

	@AfterClass
	public static void cleanDAO() throws DAOException {
		UtilitTest.deleteSequence(authorDAO);
		authorDAO.delete(1L);
		authorDAO.delete(2L);
		authorDAO.delete(3L);
		authorDAO.delete(4L);
		UtilitTest.deleteSequence(newsDAO);
		newsDAO.remove(1L);
		newsDAO.remove(2L);
	}

	@Test
	public void testExpired() throws DAOException {
		authorDAO.expired(3L);
	    Author result = authorDAO.get(3L);
		assertEquals("Test3",result.getName());
		assertEquals(3L,result.getId());
		assertEquals(false,result.getExpired().equals(null));
	}
	
	@Test
	public void testUpdateNewsAuthor() throws DAOException {
		
		authorDAO.updateNewsAuthor(author3,2L);
	    Author result = authorDAO.getAuthorByNewsId(2L);		
		assertEquals("Test3",result.getName());
		assertEquals(3L,result.getId());
	}
	
	@Test
	public void testUpdate() throws DAOException {
		author2=new Author(2L,"Test21");
		authorDAO.update(author2);
	    Author result = authorDAO.get(2L);
		assertEquals("Test21",result.getName());
		assertEquals(2L,result.getId());
	}
	
	@Test
	public void testAdd() throws DAOException {
		authorDAO.add(author4);
		List<Author> result = authorDAO.getAll();
		assertEquals(true, result.contains(author4));
	}
	
	@Test
	public void testAddAuthorNews() throws DAOException {
		authorDAO.add(author2,1L);
		Author result = authorDAO.getAuthorByNewsId(1L);		
		assertEquals(author2, result);
	}

	@Test
	public void testGetAuthorByNewsId() throws DAOException {
		Author result = authorDAO.getAuthorByNewsId(1L);		
		assertEquals(author2, result);
	}
	
	@Test
	public void testGet() throws DAOException {
		Author result = authorDAO.get(1L);
		assertEquals(author1, result);
	}

	@Test
	public void testGetAll() throws DAOException {
		List<Author> result = authorDAO.getAll();

		assertEquals(true, result.contains(author1));
		assertEquals(true, result.contains(author2));
	}
}
