package com.epam.muha.dao;

import static org.junit.Assert.assertEquals;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.muha.dao.impl.AuthorDAO;
import com.epam.muha.dao.impl.CommentDAO;
import com.epam.muha.dao.impl.NewsDAO;
import com.epam.muha.dao.impl.PageDAO;
import com.epam.muha.dao.impl.TagDAO;
import com.epam.muha.entity.Author;
import com.epam.muha.entity.Comment;
import com.epam.muha.entity.News;
import com.epam.muha.entity.Tag;
import com.epam.muha.exception.DAOException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:springpropertiestest.xml" })
public class PageDAOTest {

	//@Autowired
	//@Qualifier("NewsDAO")
	private static INewsDAO newsDAO;
	//@Autowired
	//@Qualifier("TagDAO")
	private static ITagDAO tagDAO ;
	//@Autowired
	//@Qualifier("CommentDAO")
	private static ICommentDAO commentDAO;
	//@Autowired
	//@Qualifier("AuthorDAO")
	private static IAuthorDAO authorDAO;
	
	private static IPageDAO pageDAO;
	
	
	public static java.util.Date today = new java.util.Date();
	public static Timestamp date=new Timestamp(today.getTime());
	
	private static News news1;
	private static News news2;
	private static News news3;
		
	private static Tag tag1;
	private static Tag tag2;
	private static Tag tag3;
	
	private static Comment comment1;
	private static Comment comment2;
	private static Comment comment3;
	private static Comment comment4;
	
	private static Author author1;
	private static Author author2;

	@BeforeClass
	public static void setUpDao() throws DAOException {
		@SuppressWarnings("resource")
		ClassPathXmlApplicationContext cx = new ClassPathXmlApplicationContext("springpropertiestest.xml");
		Locale.setDefault(Locale.ENGLISH);
		commentDAO=(CommentDAO) cx.getBean("commentDAO");
		newsDAO=(NewsDAO) cx.getBean("newsDAO");
		authorDAO=(AuthorDAO) cx.getBean("authorDAO");
		tagDAO=(TagDAO) cx.getBean("tagDAO");
		pageDAO=(PageDAO) cx.getBean("pageDAO");
		tag1 = new Tag(1L,"Test1");
		tag2 = new Tag(2L,"Test2");
		tag3 = new Tag(3L,"Test3");
		
		comment1=new Comment(1L,"Comment1",date,1);
		comment2=new Comment(2L,"Comment2",date,1);
		comment3=new Comment(3L,"Comment3",date,2);
		comment4=new Comment(4L,"Comment4",date,2);
		
		author1 = new Author(1L,"Test1");
		author2 = new Author(2L,"Test2");
		
		Date dates=new Date(today.getTime());
		news1=new News(1L,"Test1","Test1","Test1",date,dates);
		news2=new News(2L,"Test2","Test2","Test2",date,dates);
		news3=new News(3L,"Test3","Test3","Test3",date,dates);
		
		UtilitTest.createSequence(newsDAO);
		newsDAO.add(news1);
		newsDAO.add(news2);
		newsDAO.add(news3);
		
		UtilitTest.createSequence(tagDAO);
		tagDAO.add(tag1);
		tagDAO.add(tag2);
		tagDAO.add(tag3);
		
    	tagDAO.add(tag1.getId(),1L);
		tagDAO.add(tag2.getId(),1L);
		tagDAO.add(tag2.getId(),2L);
		
		UtilitTest.createSequence(commentDAO);
		commentDAO.add(comment1);
		commentDAO.add(comment2);
		commentDAO.add(comment3);
		commentDAO.add(comment4);
		
		UtilitTest.createSequence(authorDAO);
		authorDAO.add(author1);
		authorDAO.add(author2);
		
		authorDAO.add(author1,1L);
		authorDAO.add(author2,2L);
	}

	@AfterClass
	public static void cleanDAO() throws DAOException {
		UtilitTest.deleteSequence(commentDAO);
		List<Comment> comments = commentDAO.getAll(1L);
		for (Comment comment : comments) {
			commentDAO.remove(comment.getId());
		}
	    comments = commentDAO.getAll(2L);
		for (Comment comment : comments) {
			commentDAO.remove(comment.getId());
		}
		UtilitTest.deleteSequence(tagDAO);
		tagDAO.remove(1L);
		tagDAO.remove(2L);
		tagDAO.remove(3L);
		
		UtilitTest.deleteSequence(authorDAO);
		authorDAO.delete(1L);
		authorDAO.delete(2L);
		
		UtilitTest.deleteSequence(newsDAO);
		newsDAO.remove(1L);
		newsDAO.remove(2L);
		newsDAO.remove(3L);
		newsDAO.remove(4L);
	}
	

	@Test
	public void testGetFilterList() throws DAOException {		
		List<Tag> tags=new ArrayList<Tag>();
		List<Author> authors=new ArrayList<Author>();
		tags.add(tag1);
		tags.add(tag2);	
		authors.add(author1);
		List<News> temp = pageDAO.getFilterList(tags,authors,3,1);
		assertEquals(true, temp.contains(news1));	
		assertEquals(false, temp.contains(news2));			
	}

	
	
}

