package com.epam.muha.dao;

import static org.junit.Assert.assertEquals;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;
import java.util.Locale;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.unitils.UnitilsJUnit4;

import com.epam.muha.dao.ICommentDAO;
import com.epam.muha.dao.INewsDAO;
import com.epam.muha.dao.impl.CommentDAO;
import com.epam.muha.dao.impl.NewsDAO;
import com.epam.muha.entity.Comment;
import com.epam.muha.entity.News;
import com.epam.muha.exception.DAOException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:springpropertiestest.xml" })
public class CommentDAOTest extends UnitilsJUnit4 {

	//@Autowired
	//@Qualifier("CommentDAO")
	private static ICommentDAO commentDAO;
	//@Autowired
	//@Qualifier("NewsDAO")
	private static INewsDAO newsDAO;
	
	public static java.util.Date today = new java.util.Date();
	public static Timestamp date=new Timestamp(today.getTime());
	
	private static Comment comment1;
	private static Comment comment2;
	private static Comment comment3;
	private static Comment comment4;
	private static Comment comment5;
	private static News news1;
	private static News news2;
	
	
	@SuppressWarnings("resource")
	@BeforeClass
	public static void setUpDao() throws DAOException {
		ClassPathXmlApplicationContext cx = new ClassPathXmlApplicationContext("springpropertiestest.xml");
		Locale.setDefault(Locale.ENGLISH);
		commentDAO=(CommentDAO) cx.getBean("commentDAO");
		newsDAO=(NewsDAO) cx.getBean("newsDAO");
		
		comment1=new Comment(1L,"Comment1",date,1);
		comment2=new Comment(2L,"Comment2",date,1);
		comment3=new Comment(3L,"Comment3",date,2);
		comment4=new Comment(4L,"Comment4",date,2);
		comment5=new Comment(5L,"Comment5",date,2);
		Date dates=new Date(today.getTime());
		news1=new News(1L,"Test1","Test1","Test1",date,dates);
		news2=new News(2L,"Test2","Test2","Test2",date,dates);
		
		UtilitTest.createSequence(newsDAO);
		newsDAO.add(news1);
		newsDAO.add(news2);
		
		UtilitTest.createSequence(commentDAO);
		commentDAO.add(comment1);
		commentDAO.add(comment2);
		commentDAO.add(comment3);
		commentDAO.add(comment4);
		
	}


	@AfterClass
	public static void cleanDAO() throws DAOException {
		UtilitTest.deleteSequence(commentDAO);
		UtilitTest.deleteSequence(newsDAO);
		List<Comment> comments = commentDAO.getAll(1L);
		for (Comment comment : comments) {
			commentDAO.remove(comment.getId());
		}
	    comments = commentDAO.getAll(2L);
		for (Comment comment : comments) {
			commentDAO.remove(comment.getId());
		}
		newsDAO.remove(1L);
		newsDAO.remove(2L);		
	}
	

	@Test
	public void testAdd() throws DAOException {
		commentDAO.add(comment4);
		List<Comment> result = commentDAO.getAll(2L);
		assertEquals(true,result.contains(comment4));
	}
	
	@Test
	public void testGet() throws DAOException {
		Comment result = commentDAO.get(2L);
		assertEquals("Comment2",result.getCommentText());
		assertEquals(2L,result.getId());
		assertEquals(date,result.getCreationDate());
		assertEquals(1L,result.getNewsId());
	}
	
	@Test
	public void testUpdate() throws DAOException {
		Comment temp=new Comment(3L,"Comment6",date,2);
		commentDAO.update(temp);		
		assertEquals("Comment6",commentDAO.get(3L).getCommentText());
		assertEquals(3L,commentDAO.get(3L).getId());
		assertEquals(date,commentDAO.get(3L).getCreationDate());
		assertEquals(2,commentDAO.get(3L).getNewsId());
	}
	
	@Test
	public void testRemove() throws DAOException {
		commentDAO.remove(5L);
		List<Comment> result = commentDAO.getAll(2L);
		assertEquals(false,result.contains(comment5));
	}
	
	@Test
	public void testGetAll() throws DAOException {
		List<Comment> result = commentDAO.getAll(1L);
		
		assertEquals(true,result.contains(comment1));
	    assertEquals(true,result.contains(comment2));
		assertEquals(2L,result.size());
	}


}
