package com.epam.muha.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.muha.dao.impl.NewsDAO;
import com.epam.muha.entity.News;
import com.epam.muha.entity.Tag;
import com.epam.muha.exception.DAOException;
import com.epam.muha.exception.ServiceException;
import com.epam.muha.service.impl.NewsService;

@RunWith(MockitoJUnitRunner.class)
public class NewsServiceTest {

	@Mock
	private  NewsDAO mockedNewsDAO;
	private static News news;
	private static Tag tag;
	@InjectMocks
	private INewsService newsService = new NewsService();;

	@BeforeClass
	public static void setUp() throws ServiceException {
		news = new News("Test");
		tag = new Tag();
		
	}

	@Test
	public void testSearchByTags() throws DAOException, ServiceException {
		when(mockedNewsDAO.searchByTags(Arrays.asList(tag))).thenReturn(Arrays.asList(news));
		newsService.searchByTags(Arrays.asList(tag));
		verify(mockedNewsDAO).searchByTags(Arrays.asList(tag));
		assertEquals(Arrays.asList(news),mockedNewsDAO.searchByTags(Arrays.asList(tag)));
		assertEquals(Arrays.asList(news), newsService.searchByTags(Arrays.asList(tag)));
	}
	
	@Test
	public void testSearchByAuthor() throws ServiceException, DAOException {
		long expected=2L;
		when(mockedNewsDAO.searchByAuthor(expected)).thenReturn(Arrays.asList(news));
		newsService.searchByAuthor(expected);
		verify(mockedNewsDAO).searchByAuthor(expected);
		assertEquals(Arrays.asList(news), newsService.searchByAuthor(expected));
	}
	
	@Test
	public void testAdd() throws ServiceException, DAOException {
		newsService.add(news);
		verify(mockedNewsDAO).add(news);
	}

	@Test
	public void testRemove() throws ServiceException, DAOException {
		long expected = 1L;
		newsService.remove(expected);
		verify(mockedNewsDAO).remove(expected);
	}

	@Test
	public void testGet() throws ServiceException , DAOException{
		long expected = 2L;
		when(mockedNewsDAO.get(expected)).thenReturn(news);
		newsService.get(expected);
		verify(mockedNewsDAO).get(expected);
		assertEquals(news, newsService.get(expected));

	}

	@Test
	public void testUpdate() throws ServiceException, DAOException {
		newsService.update( news);
		verify(mockedNewsDAO).update(news);

	}

	 @Test(expected=DAOException.class)
	 public void testAddDAOException() throws DAOException{
	 doThrow(new DAOException("Can't create News")).when(mockedNewsDAO).add(null);
	 mockedNewsDAO.add(null);
	 }
	 
	 @Test(expected=ServiceException.class)
	 public void testAddLogicException() throws  ServiceException, DAOException{
	 doThrow(new DAOException("Can't create News")).when(mockedNewsDAO).add(null);
	 newsService.add(null);
	 }
	 
	 @Test(expected=DAOException.class)
	 public void testRemoveDAOException() throws DAOException{
	 doThrow(new DAOException("Can't delete News")).when(mockedNewsDAO).remove(0L);
	 mockedNewsDAO.remove(0L);
	 }
	 
	 @Test(expected=ServiceException.class)
	 public void testRemoveLogicException() throws  ServiceException, DAOException{
	 doThrow(new DAOException("Can't delete News")).when(mockedNewsDAO).remove(0L);
	 newsService.remove(0L);
	 }
	 
	 @Test(expected=DAOException.class)
	 public void testGetDAOException() throws DAOException{
	 doThrow(new DAOException("Can't read News")).when(mockedNewsDAO).get(1L);
	 mockedNewsDAO.get(1L);
	 }
	 
	 @Test(expected=ServiceException.class)
	 public void testGetLogicException() throws  ServiceException, DAOException{
	 doThrow(new DAOException("Can't read News")).when(mockedNewsDAO).get(1L);
	 newsService.get(1L);
	 }
	
	 @Test(expected=DAOException.class)
	 public void testUpdateDAOException() throws DAOException{
	 doThrow(new DAOException("Can't update News")).when(mockedNewsDAO).update(news);
	 mockedNewsDAO.update(news);
	 }
	 
	 @Test(expected=ServiceException.class)
	 public void testUpdateLogicException() throws  ServiceException, DAOException{
	 doThrow(new DAOException("Can't update News")).when(mockedNewsDAO).update(news);
	 newsService.update(news);
	 }
	
	 @Test(expected=DAOException.class)
	 public void testGetAllDAOException() throws DAOException{
	 doThrow(new DAOException("Can't get all News")).when(mockedNewsDAO).getAllNews();
	 mockedNewsDAO.getAllNews();
	 }
	 
	 @Test(expected=ServiceException.class)
	 public void testGetAllLogicException() throws  ServiceException, DAOException{
	 doThrow(new DAOException("Can't get all News")).when(mockedNewsDAO).getAllNews();
	 newsService.getAllNews();
	 }
	 @Test(expected=DAOException.class)
	 public void testSearchByAuthorDAOException() throws DAOException{
	 doThrow(new DAOException("Can't search Author")).when(mockedNewsDAO).searchByAuthor(1L);
	 mockedNewsDAO.searchByAuthor(1L);
	 }
	 
	 @Test(expected=ServiceException.class)
	 public void testSearchByAuthorLogicException() throws  ServiceException, DAOException{
	 doThrow(new DAOException("Can't search Author")).when(mockedNewsDAO).searchByAuthor(1L);
	 newsService.searchByAuthor(1L);	 
	 }	
	 
	 
	 @Test(expected=DAOException.class)
	 public void testSearchByTagsDAOException() throws DAOException{
	 doThrow(new DAOException("Can't search Tag")).when(mockedNewsDAO).searchByTags(null);
	 mockedNewsDAO.searchByTags(null);
	 }
	 
	 @Test(expected=ServiceException.class)
	 public void testSearchByTagsLogicException() throws  ServiceException, DAOException{
	 doThrow(new DAOException("Can't search Tag")).when(mockedNewsDAO).searchByTags(null);
     newsService.searchByTags(null);
	 }
}
