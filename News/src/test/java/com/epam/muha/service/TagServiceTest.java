package com.epam.muha.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.muha.dao.impl.TagDAO;
import com.epam.muha.entity.Tag;
import com.epam.muha.exception.DAOException;
import com.epam.muha.exception.ServiceException;
import com.epam.muha.service.impl.TagService;

@RunWith(MockitoJUnitRunner.class)
public class TagServiceTest {

	@Mock
	private  TagDAO mockedTagDAO;
	private static Tag tag;
	@InjectMocks
	private ITagService tagService= new TagService();

	@BeforeClass
	public static void setUp() {
		tag = new Tag();
	}

	@Test
	public void testAdd() throws DAOException, ServiceException {
		tagService.add(tag);
		verify(mockedTagDAO).add(tag);
	}

	@Test
	public void testAddWithNewsId() throws ServiceException, DAOException {
		long expected = 1L;
		tagService.add(expected, expected);
		verify(mockedTagDAO).add(expected, expected);
	}

	@Test
	public void testRemove() throws ServiceException, DAOException{
		long expected = 1L;
		tagService.remove(expected);
		verify(mockedTagDAO).remove(expected);
	}

	public void testRemoveTagFromNews() throws ServiceException, DAOException {
		tagService.remove(anyLong(), anyLong());
		verify(mockedTagDAO).remove(anyLong(), anyLong());
	}

	@Test
	public void testUpdate() throws DAOException, ServiceException {
		tagService.update(tag);
		verify(mockedTagDAO).update(tag);

	}

	@Test
	public void testGet() throws DAOException, ServiceException {
		long expected =0L;
		when(mockedTagDAO.get(expected)).thenReturn(tag);
		tagService.get(expected);
		verify(mockedTagDAO).get(expected);
		assertEquals(tag, mockedTagDAO.get(expected));
		assertEquals(tag, tagService.get(expected));

	}
	
	@Test
	public void testGetByNewsId() throws DAOException, ServiceException {
		long expected = 3L;
		when(mockedTagDAO.getTagsByNews(expected)).thenReturn(Arrays.asList(tag));
		tagService.getTagsByNews(expected);
		verify(mockedTagDAO).getTagsByNews(expected);
		assertEquals(Arrays.asList(tag), mockedTagDAO.getTagsByNews(expected));
		assertEquals(Arrays.asList(tag), tagService.getTagsByNews(expected));

	}

	 @Test(expected=DAOException.class)
	 public void testAddException() throws DAOException{
	 doThrow(new DAOException("Can't create tag")).when(mockedTagDAO).add(null);
	 mockedTagDAO.add(null);
	 }
	 
	 @Test(expected=ServiceException.class)
	 public void testAddLogicException() throws  ServiceException, DAOException{
	 doThrow(new DAOException("Can't create tag")).when(mockedTagDAO).add(null);
	 tagService.add(null);
	 }
	 
	 @Test(expected=DAOException.class)
	 public void testAddWithNewsIdException() throws DAOException{
	 doThrow(new DAOException("Can't create tag in news")).when(mockedTagDAO).add(0L,0L);
	 mockedTagDAO.add(0L,0L);
	 }
	 
	 @Test(expected=ServiceException.class)
	 public void testAddWithNewsIdLogicException() throws  ServiceException, DAOException{
	 doThrow(new DAOException("Can't create tag in news")).when(mockedTagDAO).add(0L,0L);
	 tagService.add(0L,0L);
	 }
	 
	 @Test(expected=DAOException.class)
	 public void testRemoveDAOException() throws DAOException{
	 doThrow(new DAOException("Can't delete News")).when(mockedTagDAO).remove(0L);
	 mockedTagDAO.remove(0L);
	 }
	 
	 @Test(expected=ServiceException.class)
	 public void testRemoveLogicException() throws  ServiceException, DAOException{
	 doThrow(new DAOException("Can't delete Tag")).when(mockedTagDAO).remove(0L);
	 tagService.remove(0L);
	 }
	 
	 @Test(expected=DAOException.class)
	 public void testRemoveTagFromNewsDAOException() throws DAOException{
	 doThrow(new DAOException("Can't delete News")).when(mockedTagDAO).remove(0L,0L);
	 mockedTagDAO.remove(0L,0L);
	 }
	 
	 @Test(expected=ServiceException.class)
	 public void testRemoveTagFromNewsLogicException() throws  ServiceException, DAOException{
	 doThrow(new DAOException("Can't delete Tag")).when(mockedTagDAO).remove(0L,0L);
	 tagService.remove(0L,0L);
	 }
	 
	 @Test(expected=DAOException.class)
	 public void testGetDAOException() throws DAOException{
	 doThrow(new DAOException("Can't read Tag")).when(mockedTagDAO).get(1L);
	 mockedTagDAO.get(1L);
	 }
	 
	 @Test(expected=ServiceException.class)
	 public void testGetLogicException() throws  ServiceException, DAOException{
	 doThrow(new DAOException("Can't read Tag")).when(mockedTagDAO).get(1L);
	 tagService.get(1L);
	 }
	
	 @Test(expected=DAOException.class)
	 public void testGetTagsByNewsDAOException() throws DAOException{
	 doThrow(new DAOException("Can't read Tag")).when(mockedTagDAO).getTagsByNews(1L);
	 mockedTagDAO.getTagsByNews(1L);
	 }
	 
	 @Test(expected=ServiceException.class)
	 public void testGetTagsByNewsLogicException() throws  ServiceException, DAOException{
	 doThrow(new DAOException("Can't read Tag")).when(mockedTagDAO).getTagsByNews(1L);
	 tagService.getTagsByNews(1L);
	 }
	
	 
	 @Test(expected=DAOException.class)
	 public void testUpdateDAOException() throws DAOException{
	 doThrow(new DAOException("Can't update Tag")).when(mockedTagDAO).update(tag);
	 mockedTagDAO.update(tag);
	 }
	 
	 @Test(expected=ServiceException.class)
	 public void testUpdateLogicException() throws  ServiceException, DAOException{
	 doThrow(new DAOException("Can't update Tag")).when(mockedTagDAO).update(tag);
	 tagService.update(tag);
	 }
	
	 @Test(expected=DAOException.class)
	 public void testGetAllDAOException() throws DAOException{
	 doThrow(new DAOException("Can't get all Tag")).when(mockedTagDAO).getAll();
	 mockedTagDAO.getAll();
	 }
	 
	 @Test(expected=ServiceException.class)
	 public void testGetAllLogicException() throws  ServiceException, DAOException{
	 doThrow(new DAOException("Can't get all Tag")).when(mockedTagDAO).getAll();
	 tagService.getAll();
	 }

	 
}
