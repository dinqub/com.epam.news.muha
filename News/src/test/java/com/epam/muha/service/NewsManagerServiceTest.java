package com.epam.muha.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.muha.entity.Author;
import com.epam.muha.entity.News;
import com.epam.muha.entity.NewsVO;
import com.epam.muha.entity.Tag;
import com.epam.muha.exception.ServiceException;
import com.epam.muha.service.impl.NewsManagerService;

@RunWith(MockitoJUnitRunner.class)
public class NewsManagerServiceTest {
	

	private static Tag tag;
	private static Author author;	
	private static NewsVO news;	
	
	@Mock
	private ITagService mokedTagService;
	@Mock
	private IAuthorService mokedAuthorService;
	@Mock
	private INewsService mokedNewsService;
	@Mock
	private ICommentService mokedCommentService;
	
	@InjectMocks
	private INewsManagerService newsManagerService= new NewsManagerService();

	@BeforeClass
	public static void setUp() {
				
		tag = new Tag("Tag");
		author=new Author("Test");
		news=new NewsVO();
		news.setAuthors(author);
		news.setTags(Arrays.asList(tag));
		news.setNews(new News("Test"));


	}

    @Test
	public void testSearchNewsByTags() throws ServiceException, ServiceException {
		News temp=new News(); 
	    when(mokedNewsService.searchByTags(Arrays.asList(tag))).thenReturn(Arrays.asList(temp));
		newsManagerService.searchNewsByTags(Arrays.asList(tag));
		verify(mokedNewsService).searchByTags(Arrays.asList(tag));
			
	}
		
	@Test
	public void testSearchNewsByAuthor() throws ServiceException, ServiceException {
		long expected=2;
		News temp=new News(); 
		temp.setId(120);
		when(mokedNewsService.searchByAuthor(expected)).thenReturn(Arrays.asList(temp));
		newsManagerService.searchNewsByAuthor(expected);
		verify(mokedNewsService).searchByAuthor(expected);
		verify(mokedAuthorService).getAuthorByNewsId(temp.getId());
		verify(mokedTagService).getTagsByNews(temp.getId());
		verify(mokedCommentService).getAll(temp.getId());
	}
				
	@Test
	public void testAddNews() throws ServiceException, ServiceException {
		newsManagerService.addNews(news);
		verify(mokedNewsService).add(news.getNews());
		verify(mokedAuthorService).add(news.getAuthors(),news.getNews().getId());
		for(int i=0;i<news.getTags().size();++i)
		verify(mokedTagService).add(news.getTags().get(i).getId(),news.getNews().getId());
	}

	@Test
	public void testRemoveNews() throws ServiceException, ServiceException {
		long expected = 1L;
		when(mokedAuthorService.getAuthorByNewsId(expected)).thenReturn(news.getAuthors());
		newsManagerService.removeNews(expected);
		verify(mokedNewsService).remove(expected);
	}

	@Test
	public void testGetNews() throws ServiceException , ServiceException{
		long expected = news.getNews().getId();
		when(mokedNewsService.get(expected)).thenReturn(news.getNews());
		when(mokedAuthorService.getAuthorByNewsId(expected)).thenReturn(news.getAuthors());
		when(mokedTagService.getTagsByNews(expected)).thenReturn(news.getTags());
		when(mokedCommentService.getAll(expected)).thenReturn(news.getComments());
		newsManagerService.getNews(expected);
	    verify(mokedNewsService).get(expected);
	//	verify(mockedAuthorDAO).getAuthorByNewsId(expected);
	//	verify(mokedTagService).getTagsByNews(expected);
	//	verify(mokedCommentService).getAll(expected);
		assertEquals(news.getNews(), newsManagerService.getNews(expected).getNews());
		assertEquals(news.getAuthors(), newsManagerService.getNews(expected).getAuthors());
		assertEquals(news.getComments(), newsManagerService.getNews(expected).getComments());
		assertEquals(news.getTags(), newsManagerService.getNews(expected).getTags());

	}

	@Test
	public void testUpdateNews() throws ServiceException, ServiceException {
		newsManagerService.updateNews(news);
		verify(mokedNewsService).update( news.getNews());
	}

    @Test(expected=ServiceException.class)
    public void testAddNewsServiceException() throws ServiceException{
        doThrow(new ServiceException("Can't create News")).when(mokedNewsService).add(null);
		 mokedNewsService.add(null);
	 }
				 
    @Test(expected=ServiceException.class)
	 public void testAddNewsLogicException() throws  ServiceException, ServiceException{
		 NewsVO temp=new NewsVO();
	     temp.setNews(null);
	     doThrow(new ServiceException("Can't create News")).when(mokedNewsService).add(null);
		 newsManagerService.addNews(temp);
	 }
				 
	 @Test(expected=ServiceException.class)
	 public void testRemoveNewsServiceException() throws ServiceException{
		 doThrow(new ServiceException("Can't delete News")).when(mokedNewsService).remove(0L);
		 mokedNewsService.remove(0L);
	 }
				 
	 @Test(expected=ServiceException.class)
	 public void testRemoveNewsLogicException() throws  ServiceException, ServiceException{
		 doThrow(new ServiceException("Can't delete News")).when(mokedNewsService).remove(0L);
		 newsManagerService.removeNews(0L);
	 }
				 
	 @Test(expected=ServiceException.class)
	 public void testGetNewsServiceException() throws ServiceException{
		 doThrow(new ServiceException("Can't read News")).when(mokedNewsService).get(-1L);
		 mokedNewsService.get(-1L);
	 }
				 
	 @Test(expected=ServiceException.class)
		 public void testGetNewsLogicException() throws  ServiceException, ServiceException{
		 doThrow(new ServiceException("Can't read News")).when(mokedNewsService).get(-1L);
		 newsManagerService.getNews(-1L);
	 }
				
	 @Test(expected=ServiceException.class)
	 public void testUpdateNewsServiceException() throws ServiceException{
		 doThrow(new ServiceException("Can't update News")).when(mokedNewsService).update(null);
		 mokedNewsService.update(null);
	 }
				 
	 @Test(expected=ServiceException.class)
		 public void testUpdateNewsLogicException() throws  ServiceException, ServiceException{
		 NewsVO temp=new NewsVO();
		 temp.setNews(null);
		 doThrow(new ServiceException("Can't update News")).when(mokedNewsService).update(null);
		 newsManagerService.updateNews(temp);
	 }
				
	 @Test(expected=ServiceException.class)
	 public void testGetAllNewsServiceException() throws ServiceException{
		 doThrow(new ServiceException("Can't get all News")).when(mokedNewsService).getAllNews();
		 mokedNewsService.getAllNews();
	 }
				 
	 @Test(expected=ServiceException.class)
	 public void testGetAllNewsLogicException() throws  ServiceException, ServiceException{
		 doThrow(new ServiceException("Can't get all News")).when(mokedNewsService).getAllNews();
		 newsManagerService.getSortedNews();
	 }
				 
	 @Test(expected=ServiceException.class)
		 public void testSearchNewsByAuthorServiceException() throws ServiceException{
		 doThrow(new ServiceException("Can't search Author")).when(mokedNewsService).searchByAuthor(-1L);
		 mokedNewsService.searchByAuthor(-1L);
	 }
			 
	 @Test(expected=ServiceException.class)
	 public void testSearchNewsByAuthorLogicException() throws  ServiceException, ServiceException{
		 doThrow(new ServiceException("Can't search Author")).when(mokedNewsService).searchByAuthor(-1L);
		 newsManagerService.searchNewsByAuthor(-1L);	 
	 }	
				 
	 @Test(expected=ServiceException.class)
	 public void testSearchNewsByTagsServiceException() throws  ServiceException{
		 doThrow(new ServiceException("Can't search Tag")).when(mokedNewsService).searchByTags(null);
		 mokedNewsService.searchByTags(null);
	 }
				 
	 @Test(expected=ServiceException.class)
	 public void testSearchNewsByTagsLogicException() throws  ServiceException, ServiceException{
		 doThrow(new ServiceException("Can't search Tag")).when(mokedNewsService).searchByTags(null);
	     newsManagerService.searchNewsByTags(null);
	 }
}
