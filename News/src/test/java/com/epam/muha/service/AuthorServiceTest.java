package com.epam.muha.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.muha.dao.impl.AuthorDAO;
import com.epam.muha.entity.Author;
import com.epam.muha.exception.DAOException;
import com.epam.muha.exception.ServiceException;
import com.epam.muha.service.impl.AuthorService;

@RunWith(MockitoJUnitRunner.class)
public class AuthorServiceTest {

	@Mock
	private AuthorDAO mockedAuthorDAO;
	private static Author author;
	@InjectMocks
	private IAuthorService authorService=new AuthorService();

	@BeforeClass
	public static void setUp() {
		author = new Author("Author");				
	}

	@Test
	public void testAdd() throws DAOException, ServiceException {
		authorService.add(author);
		verify(mockedAuthorDAO).add(author);
	}
	
	@Test
	public void testAddToNews() throws DAOException, ServiceException {
		authorService.add(author,0L);
		verify(mockedAuthorDAO).add(author,0L);
	}

	@Test
	public void testExpired() throws ServiceException, DAOException {
		long expected=1L;
		authorService.expired(expected);
		verify(mockedAuthorDAO).expired(expected);
	}
	@Test
	public void testGet() throws DAOException, ServiceException {
		long expected=2L;
		when(mockedAuthorDAO.get(expected)).thenReturn(author);
		authorService.get(expected);
		verify(mockedAuthorDAO).get(expected);
		assertEquals(author, authorService.get(expected));
	}
	
	@Test
	public void testGetByNewsId() throws DAOException, ServiceException {
		long expected=2L;
		when(mockedAuthorDAO.getAuthorByNewsId(expected)).thenReturn(author);
		authorService.getAuthorByNewsId(expected);
		verify(mockedAuthorDAO).getAuthorByNewsId(expected);
		assertEquals(author, authorService.getAuthorByNewsId(expected));
	}

	@Test
	public void testUpdate() throws DAOException, ServiceException {
		authorService.update(author);
		verify(mockedAuthorDAO).update(author);
	}
	
	@Test
	public void testUpdateNewsAuthor() throws DAOException, ServiceException {
		long expected=1L;
		authorService.updateNewsAuthor(author, expected);
		verify(mockedAuthorDAO).updateNewsAuthor(author, expected);
	}

	 @Test(expected=DAOException.class)
	 public void testAddException() throws DAOException{
	 doThrow(new DAOException("Can't create Author")).when(mockedAuthorDAO).add(null);
	 mockedAuthorDAO.add(null);
	 }
	 
	 @Test(expected=ServiceException.class)
	 public void testAddLogicException() throws  ServiceException, DAOException{
	 doThrow(new DAOException("Can't create Author")).when(mockedAuthorDAO).add(null);
	 authorService.add(null);
	 }
	 
	 @Test(expected=DAOException.class)
	 public void testAddNewsException() throws DAOException{
	 doThrow(new DAOException("Can't create Author")).when(mockedAuthorDAO).add(null,0L);
	 mockedAuthorDAO.add(null,0L);
	 }
	 
	 @Test(expected=ServiceException.class)
	 public void testAddNewsLogicException() throws  ServiceException, DAOException{
	 doThrow(new DAOException("Can't create Author")).when(mockedAuthorDAO).add(null,0L);
	 authorService.add(null,0L);
	 }
	 
	 
	 @Test(expected=DAOException.class)
	 public void testRemoveDAOException() throws DAOException{
	 doThrow(new DAOException("Can't delete News")).when(mockedAuthorDAO).expired(0L);
	 mockedAuthorDAO.expired(0L);
	 }
	 
	 @Test(expected=ServiceException.class)
	 public void testRemoveLogicException() throws  ServiceException, DAOException{
	 doThrow(new DAOException("Can't delete Author")).when(mockedAuthorDAO).expired(-1L);
	 authorService.expired(-1L);
	 }
	 
	 @Test(expected=DAOException.class)
	 public void testGetDAOException() throws DAOException{
	 doThrow(new DAOException("Can't read Author")).when(mockedAuthorDAO).get(1L);
	 mockedAuthorDAO.get(1L);
	 }
	 
	 @Test(expected=ServiceException.class)
	 public void testGetLogicException() throws  ServiceException, DAOException{
	 doThrow(new DAOException("Can't read Author")).when(mockedAuthorDAO).get(1L);
	 authorService.get(1L);
	 }
	 
	 @Test(expected=DAOException.class)
	 public void testGetAuthorByNewsIDAOException() throws DAOException{
	 doThrow(new DAOException("Can't read Author")).when(mockedAuthorDAO).getAuthorByNewsId(0L);
	 mockedAuthorDAO.getAuthorByNewsId(0L);
	 }
	 
	 @Test(expected=ServiceException.class)
	 public void testGetByNewsIdLogicException() throws  ServiceException, DAOException{
	 doThrow(new DAOException("Can't read Author")).when(mockedAuthorDAO).getAuthorByNewsId(0L);
	 authorService.getAuthorByNewsId(0L);
	 }
	
	 @Test(expected=DAOException.class)
	 public void testUpdateDAOException() throws DAOException{
	 doThrow(new DAOException("Can't update Author")).when(mockedAuthorDAO).update(author);
	 mockedAuthorDAO.update(author);
	 }
	 
	 @Test(expected=ServiceException.class)
	 public void testUpdateLogicException() throws  ServiceException, DAOException{
	 doThrow(new DAOException("Can't update Author")).when(mockedAuthorDAO).update(author);
	 authorService.update(author);
	 }
	
	 @Test(expected=DAOException.class)
	 public void testGetAllDAOException() throws DAOException{
	 doThrow(new DAOException("Can't get all Author")).when(mockedAuthorDAO).getAll();
	 mockedAuthorDAO.getAll();
	 }
	 
	 @Test(expected=ServiceException.class)
	 public void testGetAllLogicException() throws  ServiceException, DAOException{
	 doThrow(new DAOException("Can't get all Author")).when(mockedAuthorDAO).getAll();
	 authorService.getAll();
	 }
	 
}