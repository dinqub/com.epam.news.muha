package com.epam.muha.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * The Class Comment.
 */
public class Comment implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4505806203297818467L;

	/** The id. */
	private long id;

	/** The comment text. */
	private String commentText;

	/** The creation date. */
	private Date creationDate;

	/** The news id. */
	private long newsId;

	/**
	 * Instantiates a new comment.
	 */
	public Comment() {
		super();
		this.id = 0;
		this.commentText = "";
		this.creationDate = new Date(1);
	}

	/**
	 * Instantiates a new comment.
	 *
	 * @param id
	 *            of Comment
	 * @param commentText
	 *            of Comment
	 * @param creationDate
	 *            of Comment
	 * @param newsId
	 *            of Comment
	 */
	public Comment(long id, String commentText, Date creationDate,
			long newsId) {
		super();
		this.id = id;
		this.commentText = commentText;
		this.creationDate = creationDate;
		this.newsId = newsId;
	}

	/**
	 * Instantiates a new comment.
	 *
	 * @param commentText
	 *            of Comment
	 * @param date
	 *            of Comment
	 */
	public Comment(String commentText, Date date) {
		this.commentText = commentText;
		this.creationDate = date;
	}

	/**
	 * Gets the id of Comment
	 *
	 * @return the id of Comment
	 */
	public long getId() {
		return id;
	}

	/**
	 * Sets the id of Comment
	 *
	 * @param id
	 *            of Comment
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Gets the comment text of Comment
	 *
	 * @return the comment text of Comment
	 */
	public String getCommentText() {
		return commentText;
	}

	/**
	 * Sets the comment text of Comment
	 *
	 * @param commentText
	 *            of Comment
	 */
	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	/**
	 * Gets the creation date of Comment
	 *
	 * @return the creation date of Comment
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * Sets the creation date of Comment
	 *
	 * @param creationDate
	 *            of Comment
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * Gets the news id of Comment
	 *
	 * @return the news id of Comment
	 */
	public long getNewsId() {
		return newsId;
	}

	/**
	 * Sets the news id of Comment
	 *
	 * @param newsId
	 *            of Comment
	 */
	public void setNewsId(long newsId) {
		this.newsId = newsId;
	}

	@Override
	public String toString() {
		return "Comment [id=" + id + ", commentText=" + commentText
				+ ", creationDate=" + creationDate + ", newsId=" + newsId + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((commentText == null) ? 0 : commentText.hashCode());
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + (int) (newsId ^ (newsId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		Comment temp = (Comment) obj;
		if (temp == null || obj == null)
			return false;
		else {
			if (this.commentText.equals(temp.commentText)
					&& this.creationDate.equals(temp.creationDate)
					&& this.id == temp.id && this.newsId == temp.newsId)
				return true;
			else
				return false;
		}
	}
}
