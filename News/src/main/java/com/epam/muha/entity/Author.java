package com.epam.muha.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * The Class Author.
 */
public class Author implements Serializable {

	private static final long serialVersionUID = 4245262789860863982L;

	/** authors id. */
	private long id;

	/** name of Author. */
	private String name;

	/** null if author work or time when he was fired. */
	private Date expired;

	/**
	 * Instantiates a new author.
	 */
	public Author() {
		this.id = 0;
		this.name = "";
		this.expired = null;
	}

	/**
	 * Instantiates a new author.
	 *
	 * @param name
	 *            of Author
	 */
	public Author(String name) {
		this.id = 0;
		this.name = name;
		this.expired = null;
	}

	/**
	 * Instantiates a new author.
	 *
	 * @param l
	 *            of Author
	 * @param name
	 *            of Author
	 */
	public Author(long l, String name) {
		this.id = l;
		this.name = name;
	}

	/**
	 * Gets the id of Author
	 *
	 * @return the id of Author
	 */
	public long getId() {
		return id;
	}

	/**
	 * Sets the id of Author
	 *
	 * @param id
	 *            of Author
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Gets the name of Author
	 *
	 * @return the name of Author
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name of Author
	 *
	 * @param name
	 *            of Author
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the expired of Author
	 *
	 * @return the expired of Author
	 */
	public Date getExpired() {
		return expired;
	}

	/**
	 * Sets the expired of Author
	 *
	 * @param expired
	 *            of Author
	 */
	public void setExpired(Date expired) {
		this.expired = expired;
	}

	@Override
	public String toString() {
		return "Author [id=" + id + ", name=" + name + ", expired=" + expired
				+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((expired == null) ? 0 : expired.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		Author temp = (Author) obj;
		if (temp == null || obj == null)
			return false;
		else {
			if (temp.expired == null && this.expired == null) {
				if (this.id == temp.id && this.name.equals(temp.name))
					return true;
				else
					return false;
			} else
				return false;
		}
	}
}
