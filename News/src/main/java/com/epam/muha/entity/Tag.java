package com.epam.muha.entity;

import java.io.Serializable;

/**
 * The Class Tag.
 */
public class Tag implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 446589848289653937L;

	/** The id of Tag. */
	private long id;

	/** The name of Tag. */
	private String name;

	/**
	 * Instantiates a new tag .
	 */
	public Tag() {
		this.id = 0;
		this.name = "";
	}

	/**
	 * Instantiates a new tag.
	 *
	 * @param id
	 *            of Tag
	 * @param name
	 *            of Tag
	 */
	public Tag(long id, String name) {
		this.id = id;
		this.name = name;
	}

	/**
	 * Instantiates a new tag.
	 *
	 * @param name
	 *            of Tag
	 */
	public Tag(String name) {
		this.name = name;
	}

	/**
	 * Gets the id of Tag
	 *
	 * @return the id of Tag
	 */
	public long getId() {
		return id;
	}

	/**
	 * Sets the id of Tag
	 *
	 * @param id
	 *            of Tag
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Gets the name of Tag
	 *
	 * @return the name of Tag
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name of Tag
	 *
	 * @param name
	 *            of Tag
	 */
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Tag [id=" + id + ", name=" + name + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		Tag temp = (Tag) obj;
		if (temp == null || obj == null)
			return false;
		else {
			if (this.id == temp.id && this.name.equals(temp.name))
				return true;
			else
				return false;
		}
	}
}
