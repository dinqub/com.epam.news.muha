package com.epam.muha.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class NewsShell.
 */
public class NewsVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9204503801285102099L;

	/** The news. */
	private News news;
	
	/** The list of tags. */
	private List<Tag> tags;
	
	/** The list of comments. */
	private List<Comment> comments;
	
	/** The author of NewsShell. */
	private Author author;

	/**
	 * Instantiates a new news shell.
	 */
	public NewsVO() {
		this.news = new News();
		this.tags = new ArrayList<Tag>();
		this.comments = new ArrayList<Comment>();
		this.author = new Author();
	}
	
	/**
	 * Instantiates a new news shell.
	 *
	 * @param news the news
	 */
	public NewsVO(News news) {
		this.news = news;
		this.tags = new ArrayList<Tag>();
		this.comments = new ArrayList<Comment>();
		this.author = new Author();
	}

	/**
	 * Instantiates a new news shell.
	 *
	 * @param news of NewsShell
	 * @param list of tag of NewsShell
	 * @param list of comment of NewsShell
	 * @param authors of NewsShell
	 */
	public NewsVO(News news, List<Tag> tags, List<Comment> comments,
			Author authors) {
		this.news = news;
		this.tags = tags;
		this.comments = comments;
		this.author = authors;
	}

	/**
	 * Gets the tags of NewsShell
	 *
	 * @return the tags of NewsShell
	 */
	public List<Tag> getTags() {
		return tags;
	}

	/**
	 * Sets the tags of NewsShell
	 *
	 * @param tags of NewsShell
	 */
	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

	/**
	 * Gets the comments of NewsShell
	 *
	 * @return the comments of NewsShell
	 */
	public List<Comment> getComments() {
		return comments;
	}

	/**
	 * Sets the comments of NewsShell
	 *
	 * @param comments of NewsShell
	 */
	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	/**
	 * Gets the author of NewsShell
	 *
	 * @return the author of NewsShell
	 */
	public Author getAuthors() {
		return author;
	}

	/**
	 * Sets the author of NewsShell
	 *
	 * @param author of NewsShell
	 */
	public void setAuthors(Author authors) {
		this.author = authors;
	}

	/**
	 * Gets the news of NewsShell
	 *
	 * @return the news of NewsShell
	 */
	public News getNews() {
		return news;
	}

	/**
	 * Sets the news of NewsShell
	 *
	 * @param news of NewsShell
	 */
	public void setNews(News news) {
		this.news = news;
	}

	@Override
	public String toString() {
		return "NewsShell [news=" + news + ", tags=" + tags + ", comments="
				+ comments + ", author=" + author + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((author == null) ? 0 : author.hashCode());
		result = prime * result
				+ ((comments == null) ? 0 : comments.hashCode());
		result = prime * result + ((news == null) ? 0 : news.hashCode());
		result = prime * result + ((tags == null) ? 0 : tags.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NewsVO other = (NewsVO) obj;
		if (author == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author))
			return false;
		if (comments == null) {
			if (other.comments != null)
				return false;
		} else if (!comments.equals(other.comments))
			return false;
		if (news == null) {
			if (other.news != null)
				return false;
		} else if (!news.equals(other.news))
			return false;
		if (tags == null) {
			if (other.tags != null)
				return false;
		} else if (!tags.equals(other.tags))
			return false;
		return true;
	}
	
}
