package com.epam.muha.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * The Class News.
 */

public class News implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4323006207752938575L;

	/** The id of News */
	private long id;

	/** The short text of News */
	private String shortText;

	/** The full text of News */
	private String fullText;

	/** The title of News */
	private String title;

	/** The creation date of News */
	private Date creationDate;

	/** The modification date of News */
	private Date modificationDate;

	/**
	 * Instantiates a new news.
	 */
	public News() {
		this.id = 0;
		this.shortText = "";
		this.fullText = "";
		this.title = "";
		this.creationDate = null;
		this.modificationDate = null;
	}

	/**
	 * Instantiates a new news.
	 *
	 * @param name
	 *            of news
	 */
	public News(String news) {
		this.id = 0;
		this.shortText = news;
		this.fullText = news;
		this.title = news;
		this.creationDate = new Date(1);
		this.modificationDate = new Date(1);
	}

	/**
	 * Instantiates a new news.
	 *
	 * @param id
	 *            of News
	 * @param shortText
	 *            of News
	 * @param fullText
	 *            of News
	 * @param title
	 *            of News
	 * @param creationDate
	 *            of News
	 * @param modificationDate
	 *            of News
	 */
	public News(long id, String shortText, String fullText, String title,
			Date creationDate, Date modificationDate) {
		super();
		this.id = id;
		this.shortText = shortText;
		this.fullText = fullText;
		this.title = title;
		this.creationDate = creationDate;
		this.modificationDate = modificationDate;
	}

	/**
	 * Gets the short text of News
	 *
	 * @return the short text of News
	 */
	public String getShortText() {
		return shortText;
	}

	/**
	 * Sets the short text of News
	 *
	 * @param shortText
	 *            of News
	 */
	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	/**
	 * Gets the full text of News
	 *
	 * @return the full text of News
	 */
	public String getFullText() {
		return fullText;
	}

	/**
	 * Sets the full text of News
	 *
	 * @param fullText
	 *            of News
	 */
	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	/**
	 * Gets the title of News
	 *
	 * @return the title of News
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title of News
	 *
	 * @param title
	 *            of News
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the id of News
	 *
	 * @return the id of News
	 */
	public long getId() {
		return id;
	}

	/**
	 * Sets the id of News
	 *
	 * @param id
	 *            of News
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Gets the creation date of News
	 *
	 * @return the creation date of News
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * Sets the creation date of News
	 *
	 * @param creationDate
	 *            of News
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * Gets the modification date of News
	 *
	 * @return the modification date of News
	 */
	public Date getModificationDate() {
		return modificationDate;
	}

	/**
	 * Sets the modification date of News
	 *
	 * @param modificationDate
	 *            of News
	 */
	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	@Override
	public String toString() {
		return "News [id=" + id + ", shortText=" + shortText + ", fullText="
				+ fullText + ", title=" + title + ", creationDate="
				+ creationDate + ", modificationDate=" + modificationDate + "]\n";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result
				+ ((fullText == null) ? 0 : fullText.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime
				* result
				+ ((modificationDate == null) ? 0 : modificationDate.hashCode());
		result = prime * result
				+ ((shortText == null) ? 0 : shortText.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		News news = (News) obj;
		if (news == null || obj == null)
			return false;
		else {
			if (this.id == news.id && this.fullText.equals(news.fullText)
					&& this.shortText.equals(news.shortText)
					&& this.title.equals(news.title)
					&& this.creationDate.equals(news.creationDate))
				return true;
			else
				return false;

		}
	}

}
