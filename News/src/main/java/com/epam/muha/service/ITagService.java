package com.epam.muha.service;

import java.util.List;

import com.epam.muha.dao.ITagDAO;
import com.epam.muha.entity.Tag;
import com.epam.muha.exception.ServiceException;

/**
 * The Interface ITagLogic.
 */
public interface ITagService {
	
	/**
	 * Add tag in news
	 *
	 * @param tagId - id of tag
	 * @param id - id of news
	 * @throws ServiceException the technical logic exception
	 */
	public void add(long tagId,long id) throws ServiceException;
	
	/**
	 * Adds tag
	 *
	 * @param tag - tag
	 * @throws ServiceException the technical logic exception
	 */
	public void add(Tag tag) throws  ServiceException;

	/**
	 * Removes tag
	 *
	 * @param id - id of removing tag
	 * @throws ServiceException the technical logic exception
	 */
	public void remove(long id) throws ServiceException;
	
	/**
	 * Removes tag from news
	 *
	 * @param newsId - id of news
	 * @param id - id of removing tag
	 * @throws ServiceException the technical logic exception
	 */
	public void remove(long newsId,long id) throws ServiceException;

	/**
	 * Gets tag by id
	 *
	 * @param id - id of tag
	 * @return tag by id
	 * @throws ServiceException the technical logic exception
	 */
	public Tag get(long id) throws ServiceException ;
	
	/**
	 * Gets all tag by id of news
	 *
	 * @param newsId - id of news
	 * @return list of tags that contains news
	 * @throws ServiceException the technical logic exception
	 */
	public List<Tag> getTagsByNews(long newsId) throws ServiceException;
	
	/**
	 * Gets the all tag
	 *
	 * @return list of all tag
	 * @throws ServiceException the technical logic exception
	 */
	public List<Tag> getAll() throws ServiceException ;
	
	/**
	 * Update tag by id of tag
	 *
	 * @param tag - news tag
	 * @throws ServiceException the technical logic exception
	 */
	public void update(Tag tag) throws ServiceException;
	
	/**
	 * Update list of tag for news
	 * @param newsId - id of updating news
	 * @param tags - list of updating tags
	 */
	public void updateAllTagByNews(long newsId, List<Tag> tags) throws ServiceException;

    /**
     * Sets the dao.
     *
     * @param TagDAO the new dao
     */
    public void setTagDAO(ITagDAO tagDAO);
}
