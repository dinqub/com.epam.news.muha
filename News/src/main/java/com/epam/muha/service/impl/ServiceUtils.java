package com.epam.muha.service.impl;

import com.epam.muha.entity.Author;
import com.epam.muha.entity.Comment;
import com.epam.muha.entity.News;
import com.epam.muha.entity.Tag;
import com.epam.muha.exception.ServiceException;

public class ServiceUtils {

	public static void assertIsExistingId(long id, String string)
			throws ServiceException {
		if (id < 0)
			throw new ServiceException("Id of " + string + " must be >0");
	}

	public static void assertNotNullEntity(Object object, String string)
			throws ServiceException {
		switch (string) {
		case "tag":
			Tag tag = (Tag) object;
			if (tag == null )
				throw new ServiceException(string + " must be not null");
			if (tag.getId() < 0)
				throw new ServiceException("Id of " + string + " must be >0");
			break;
		case "news":
			News news = (News) object;
			if (news == null)
				throw new ServiceException(string + " must be not null");
			if (news.getId() < 0)
				throw new ServiceException("Id of " + string + " must be >0");
			break;
		case "comment":
			Comment comment = (Comment) object;
			if (comment == null)
				throw new ServiceException(string + " must be not null");
			if (comment.getId() < 0)
				throw new ServiceException("Id of " + string + " must be >0");
			break;
		case "author":
			Author author = (Author) object;
			if (author == null)
				throw new ServiceException(string + " must be not null");
			if (author.getId() < 0)
				throw new ServiceException("Id of " + string + " must be >0");
			break;

		default:
			if (object == null)
				throw new ServiceException(string + " must be not null");
			break;
		}

	}

}
