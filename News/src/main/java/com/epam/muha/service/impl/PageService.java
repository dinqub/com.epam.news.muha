package com.epam.muha.service.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.epam.muha.dao.IPageDAO;
import com.epam.muha.entity.Author;
import com.epam.muha.entity.Page;
import com.epam.muha.entity.Tag;
import com.epam.muha.exception.DAOException;
import com.epam.muha.service.IPageService;


//@Service("PageService")
@Transactional
public class PageService implements IPageService {
	
	
	// @Autowired
	// @Qualifier("PageDAO")
	private IPageDAO pageDAO;


	@Override
	public Page getFilterPage(List<Author> authors, List<Tag> tags,long totalsize,long page)  {
		try {
			pageDAO.getFilterList(tags, authors,totalsize,page);
			System.out.println(pageDAO.getFilterList(tags, authors,totalsize,page));
		} catch (DAOException e) {
			//throw new ServiceException(e);
		}
		
		return null;
	}

	public IPageDAO getPageDAO() {
		return pageDAO;
	}

	public void setPageDAO(IPageDAO pageDAO) {
		this.pageDAO = pageDAO;
	}

}
