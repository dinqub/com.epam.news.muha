package com.epam.muha.service;

import java.util.List;

import com.epam.muha.entity.Author;
import com.epam.muha.entity.Page;
import com.epam.muha.entity.Tag;
import com.epam.muha.exception.DAOException;

/**
 * The Interface IPageService.
 */
public interface IPageService {
	
	/**
	 * Gets the filter page.
	 *
	 * @param authors - list of authors
	 * @param tags - list of tags
	 * @param totalsize - how much news in page
	 * @param page - number of page
	 * @return the filter page
	 * @throws DAOException the DAO exception
	 */
	public Page getFilterPage(List<Author> authors,List<Tag> tags,long totalsize,long page) throws DAOException;
	

	
}
