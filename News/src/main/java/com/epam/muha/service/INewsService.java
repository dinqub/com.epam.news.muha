package com.epam.muha.service;

import java.util.List;

import com.epam.muha.dao.INewsDAO;
import com.epam.muha.entity.News;
import com.epam.muha.entity.Tag;
import com.epam.muha.exception.ServiceException;


/**
 * The Interface INewsLogic.
 */
public interface INewsService {
	
	/**
	 * Add new news
	 *
	 * @param news - new news
	 * @return id of added news
	 * @throws ServiceException the technical logic exception
	 */
	public long add(News news) throws ServiceException;

	/**
	 * Removes news by id of news
	 *
	 * @param id - id of news
	 * @throws ServiceException the technical logic exception
	 */
	public void remove(long id) throws ServiceException;

	/**
	 * Gets news by id
	 *
	 * @param id - id of news
	 * @return news by id
	 * @throws ServiceException the technical logic exception
	 */
	public News get(long id) throws ServiceException;

	/**
	 * Update news
	 *
	 * @param news - the updating news 
	 * @throws ServiceException the technical logic exception
	 */
	public void update(News news) throws ServiceException;
	
	/**
	 * Gets all news.
	 *
	 * @return list of all news
	 * @throws ServiceException the technical logic exception
	 */
	public List<News> getAllNews() throws ServiceException;

	/**
	 * Sets the dao.
	 *
	 * @param mockedNewsDAO the new dao
	 */
	public void setNewsDAO(INewsDAO newsDAO);
	
	/**
	 * Search news by author id.
	 *
	 * @param authorId - id of author
	 * @return list of news that write required author
	 * @throws ServiceException the technical logic exception
	 */
	public List<News> searchByAuthor(long authorId) throws ServiceException;
	 
	/**
	 * Search news by tags.
	 *
	 * @param tags - list of tags for which we seek
	 * @return list of news  that contain the required tags 
	 * @throws ServiceException the technical logic exception
	 */
	public List<News> searchByTags(List<Tag> tags) throws ServiceException;
}