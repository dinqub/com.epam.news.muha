package com.epam.muha.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import com.epam.muha.dao.ITagDAO;
import com.epam.muha.entity.Tag;
import com.epam.muha.exception.DAOException;
import com.epam.muha.exception.ServiceException;
import com.epam.muha.service.ITagService;

/**
 * The Class TagLogic.
 */
// @Service("TagService")
@Transactional
public class TagService implements ITagService {

	/** The dao. */
	// @Autowired
	// @Qualifier("TagDAO")
	private ITagDAO tagDAO;

	/**
	 * Logger
	 */
	private static Logger logger = Logger.getLogger(TagService.class.getName());

	public void add(Tag tag) throws ServiceException {
		ServiceUtils.assertNotNullEntity(tag, "tag");
		try {
			tagDAO.add(tag);
		} catch (DAOException e) {
			logger.error("TechnicalDAOException (add tag failed): " + e);
			throw new ServiceException(e);
		}
	}

	public void add(long tagId, long id) throws ServiceException {
		ServiceUtils.assertIsExistingId(tagId, "tag");
		ServiceUtils.assertIsExistingId(id, "news");
		try {
			tagDAO.add(tagId, id);
		} catch (DAOException e) {
			logger.error("TechnicalDAOException (add tag in news failed): " + e);
			throw new ServiceException(e);
		}

	}

	public void remove(long id) throws ServiceException {
		ServiceUtils.assertIsExistingId(id, "tag");
		try {
			tagDAO.remove(id);
		} catch (DAOException e) {
			logger.error("TechnicalDAOException (remove tag failed): " + e);
			throw new ServiceException(e);
		}
	}

	public void remove(long newsId, long id) throws ServiceException {
		ServiceUtils.assertIsExistingId(id, "tag");
		ServiceUtils.assertIsExistingId(newsId, "news");
		try {
			tagDAO.remove(newsId, id);
		} catch (DAOException e) {
			logger.error("TechnicalDAOException (remove tag from news failed): "
					+ e);
			throw new ServiceException(e);
		}
	}

	public Tag get(long id) throws ServiceException {
		ServiceUtils.assertIsExistingId(id, "tag");
		try {
			return tagDAO.get(id);
		} catch (DAOException e) {
			logger.error("TechnicalDAOException (get tag failed): " + e);
			throw new ServiceException(e);
		}
	}

	public List<Tag> getTagsByNews(long newsId) throws ServiceException {
		ServiceUtils.assertIsExistingId(newsId, "news");
		try {
			return tagDAO.getTagsByNews(newsId);
		} catch (DAOException e) {
			logger.error("TechnicalDAOException (get tags by news id failed): "	+ e);
			throw new ServiceException(e);
		}
	}

	public void update( Tag tag) throws ServiceException {
		ServiceUtils.assertNotNullEntity(tag, "tag");
		try {
			tagDAO.update(tag);
		} catch (DAOException e) {
			logger.error("TechnicalDAOException (update tag failed): " + e);
			throw new ServiceException(e);
		}

	}

	public void updateAllTagByNews(long newsId, List<Tag> tags)	throws ServiceException {
		ServiceUtils.assertIsExistingId(newsId, "news");
		for (Tag tag : tags) {
			ServiceUtils.assertNotNullEntity(tag, "tag");
		}
		try {
			tagDAO.updateAllTagByNews(newsId, tags);
		} catch (DAOException e) {
			logger.error("TechnicalDAOException (update tag failed): " + e);
			throw new ServiceException(e);
		}

	}

	public List<Tag> getAll() throws ServiceException {
		try {
			return tagDAO.getAll();
		} catch (DAOException e) {
			logger.error("TechnicalDAOException (get all tag failed): " + e);
			throw new ServiceException(e);
		}
	}

	public void setTagDAO(ITagDAO tagDAO) {
		this.tagDAO = tagDAO;
	}

}
