package com.epam.muha.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import com.epam.muha.dao.ICommentDAO;
import com.epam.muha.entity.Comment;
import com.epam.muha.exception.DAOException;
import com.epam.muha.exception.ServiceException;
import com.epam.muha.service.ICommentService;

/**
 * The Class CommentLogic.
 */
// @Service("CommentService")
@Transactional
public class CommentService implements ICommentService {

	/** The dao. */
	// @Autowired
	// @Qualifier("CommentDAO")
	private ICommentDAO commentDAO;

	private static Logger logger = Logger.getLogger(CommentService.class
			.getName());

	public void add(Comment comment) throws ServiceException {
		ServiceUtils.assertNotNullEntity(comment, "comment");
		try {
			commentDAO.add(comment);
		} catch (DAOException e) {
			logger.error("TechnicalDAOException (add comment failed): " + e);
			throw new ServiceException(e);
		}

	}

	public void remove(long id) throws ServiceException {
		ServiceUtils.assertIsExistingId(id, "comment");
		try {
			commentDAO.remove(id);
		} catch (DAOException e) {
			logger.error("TechnicalDAOException (remove comment failed): " + e);
			throw new ServiceException(e);
		}
	}

	public void removeByNewsId(long newsId) throws ServiceException {
		ServiceUtils.assertIsExistingId(newsId, "news");
		try {
			List<Comment> comments = commentDAO.getAll(newsId);
			for (Comment comment : comments) {
				commentDAO.remove(comment.getId());
			}
		} catch (DAOException e) {
			logger.error("TechnicalDAOException (remove comment failed): " + e);
			throw new ServiceException(e);
		}
	}

	public Comment get(long id) throws ServiceException {
		ServiceUtils.assertIsExistingId(id, "comment");
		try {
			return commentDAO.get(id);
		} catch (DAOException e) {
			logger.error("TechnicalDAOException (get comment failed): " + e);
			throw new ServiceException(e);
		}
	}

	public void update(Comment comment) throws ServiceException {
		ServiceUtils.assertNotNullEntity(comment, "comment");
		try {
			commentDAO.update(comment);
		} catch (DAOException e) {
			logger.error("TechnicalDAOException (update comment failed): " + e);
			throw new ServiceException(e);
		}
	}

	public List<Comment> getAll(long id) throws ServiceException {
		ServiceUtils.assertIsExistingId(id, "comment");
		try {
			return commentDAO.getAll(id);
		} catch (DAOException e) {
			logger.error("TechnicalDAOException (get all comment failed): " + e);
			throw new ServiceException(e);
		}
	}

	public void setCommentDAO(ICommentDAO commentDAO) {
		this.commentDAO = commentDAO;
	}

}