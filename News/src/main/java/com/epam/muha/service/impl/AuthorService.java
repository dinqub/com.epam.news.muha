package com.epam.muha.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import com.epam.muha.dao.IAuthorDAO;
import com.epam.muha.entity.Author;
import com.epam.muha.exception.DAOException;
import com.epam.muha.exception.ServiceException;
import com.epam.muha.service.IAuthorService;

/**
 * The Class AuthorLogic.
 */
// @Service("AuthorService")
@Transactional
public class AuthorService implements IAuthorService {

	/** The dao. */
	// @Autowired
	// @Qualifier("AuthorDAO")
	private IAuthorDAO authorDAO;

	private static Logger logger = Logger.getLogger(AuthorService.class
			.getName());

	public void add(Author author) throws ServiceException {
		ServiceUtils.assertNotNullEntity(author, "author");
		try {
			authorDAO.add(author);
		} catch (DAOException e) {
			logger.error("TechnicalDAOException (add author failed): " + e);
			throw new ServiceException(e);
		}
	}

	public void add(Author author, long newsId) throws ServiceException {
		ServiceUtils.assertNotNullEntity(author, "author");
		ServiceUtils.assertIsExistingId(newsId, "news");
		try {
			authorDAO.add(author, newsId);
		} catch (DAOException e) {
			logger.error("TechnicalDAOException(add author in news fail): " + e);
			throw new ServiceException(e);
		}
	}

	public void expired(long id) throws ServiceException {
		ServiceUtils.assertIsExistingId(id, "author");
		try {
			authorDAO.expired(id);
		} catch (DAOException e) {
			logger.error("TechnicalDAOException(remove author fail): " + e);
			throw new ServiceException(e);
		}
	}

	public void remove(long newsId, long id) throws ServiceException {
		ServiceUtils.assertIsExistingId(newsId, "news");
		ServiceUtils.assertIsExistingId(id, "author");
		try {
			authorDAO.remove(newsId, id);
		} catch (DAOException e) {
			logger.error("TechnicalDAOException(remove author news fail):" + e);
			throw new ServiceException(e);
		}
	}

	public Author get(long id) throws ServiceException {
		ServiceUtils.assertIsExistingId(id, "author");
		try {
			return authorDAO.get(id);
		} catch (DAOException e) {
			logger.error("TechnicalDAOException (get author failed): " + e);
			throw new ServiceException(e);
		}
	}

	public Author getAuthorByNewsId(long newsId) throws ServiceException {
		ServiceUtils.assertIsExistingId(newsId, "news");
		try {
			return authorDAO.getAuthorByNewsId(newsId);
		} catch (DAOException e) {
			logger.error("TechnicalDAOException(get author by news fail): " + e);
			throw new ServiceException(e);
		}
	}

	public void update(Author author) throws ServiceException {
		ServiceUtils.assertNotNullEntity(author, "author");
		try {
			authorDAO.update(author);
		} catch (DAOException e) {
			logger.error("TechnicalDAOException(update author failed): " + e);
			throw new ServiceException(e);
		}
	}

	public void updateNewsAuthor(Author author, long newsId)
			throws ServiceException {
		ServiceUtils.assertNotNullEntity(author, "author");
		ServiceUtils.assertIsExistingId(newsId, "news");
		try {
			authorDAO.updateNewsAuthor(author, newsId);
		} catch (DAOException e) {
			logger.error("TechnicalDAOException (update author failed): " + e);
			throw new ServiceException(e);
		}
	}

	public List<Author> getAll() throws ServiceException {
		try {
			return authorDAO.getAll();
		} catch (DAOException e) {
			logger.error("TechnicalDAOException (get all author failed): " + e);
			throw new ServiceException(e);
		}
	}

	public IAuthorDAO getAuthorDAO() {
		return authorDAO;
	}

	public void setAuthorDAO(IAuthorDAO authorDAO) {
		this.authorDAO = authorDAO;
	}

}
