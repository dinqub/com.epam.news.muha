package com.epam.muha.service;

import java.util.List;

import com.epam.muha.dao.ICommentDAO;
import com.epam.muha.entity.Comment;
import com.epam.muha.exception.DAOException;
import com.epam.muha.exception.ServiceException;


/**
 * The Interface ICommentLogic.
 */
public interface ICommentService {

	/**
	 * Add new comment to news
	 *
	 * @param comment - new comment
	 * @throws ServiceException the technical logic exception
	 */
	public void add(Comment comment) throws ServiceException;

	/**
	 * Removes comment from news
	 *
	 * @param id - id of removing comment
	 * @throws ServiceException the technical logic exception
	 */
	public void remove(long id) throws ServiceException;
	
	/**
	 * Removes all comments of news from database.
	 *
	 * @param newsid - id of news
	 * @throws DAOException the technical dao exception
	 */
	public void removeByNewsId(long newsid) throws ServiceException;

	/**
	 * Gets comment by id
	 *
	 * @param id - id of comment
	 * @return  comment by id
	 * @throws ServiceException the technical logic exception
	 */
	public Comment get(long id) throws ServiceException;
	
	/**
	 * Gets the all comment of news by news id
	 *
	 * @param id - id of news
	 * @return list of all comment by news
	 * @throws ServiceException the technical logic exception
	 */
	public List<Comment> getAll(long id) throws ServiceException;
	
	/**
	 * Update comment 
	 *
	 * @param comment - new comment
	 * @throws ServiceException the technical logic exception
	 */
	public void update(Comment comment) throws ServiceException;

	/**
	 * Sets the dao.
	 *
	 * @param mockedCommentDAO the new dao
	 */
	public void setCommentDAO(ICommentDAO mockedCommentDAO);
}
