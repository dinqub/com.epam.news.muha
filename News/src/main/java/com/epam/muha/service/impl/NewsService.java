package com.epam.muha.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import com.epam.muha.dao.INewsDAO;
import com.epam.muha.entity.News;
import com.epam.muha.entity.Tag;
import com.epam.muha.exception.DAOException;
import com.epam.muha.exception.ServiceException;
import com.epam.muha.service.INewsService;

/**
 * The Class NewsLogic.
 */
// @Service("NewsService")
@Transactional
public class NewsService implements INewsService {

	/** The dao. */
	// @Autowired
	// @Qualifier("NewsDAO")
	private INewsDAO newsDAO;

	private static Logger logger = Logger
			.getLogger(NewsService.class.getName());

	public long add(News news) throws ServiceException {
		ServiceUtils.assertNotNullEntity(news, "news");
		try {
			return newsDAO.add(news);
		} catch (DAOException e) {
			logger.error("TechnicalDAOException (add news failed): " + e);
			throw new ServiceException(e);
		}
	}

	public void remove(long id) throws ServiceException {
		ServiceUtils.assertIsExistingId(id, "news");
		try {
			newsDAO.remove(id);
		} catch (DAOException e) {
			logger.error("TechnicalDAOException (remove news failed): " + e);
			throw new ServiceException(e);
		}
	}

	public News get(long id) throws ServiceException {
		ServiceUtils.assertIsExistingId(id, "news");
		try {
			return newsDAO.get(id);
		} catch (DAOException e) {
			logger.error("TechnicalDAOException (get news failed): " + e);
			throw new ServiceException(e);
		}
	}

	public void update(News news) throws ServiceException {
		ServiceUtils.assertNotNullEntity(news, "news");
		try {
			newsDAO.update( news);
		} catch (DAOException e) {
			logger.error("TechnicalDAOException (update news failed): " + e);
			throw new ServiceException(e);
		}
	}

	public List<News> getAllNews() throws ServiceException {
		try {
			return newsDAO.getAllNews();
		} catch (DAOException e) {
			logger.error("TechnicalDAOException (get all news failed): " + e);
			throw new ServiceException(e);
		}
	}

	public List<News> searchByAuthor(long authorId) throws ServiceException {
		ServiceUtils.assertIsExistingId(authorId, "author");
		try {
			return newsDAO.searchByAuthor(authorId);
		} catch (DAOException e) {
			logger.error("TechnicalDAOException(search news author fail):" + e);
			throw new ServiceException(e);
		}
	}

	public List<News> searchByTags(List<Tag> tags) throws ServiceException {
		if(tags==null)
			throw new ServiceException("List of tag must be not null");
		for (Tag tag : tags) {
			ServiceUtils.assertNotNullEntity(tag, "tag");
		}
		try {
			return newsDAO.searchByTags(tags);
		} catch (DAOException e) {
			logger.error("TechnicalDAOException (search news  tags fail):" + e);
			throw new ServiceException(e);
		}
	}

	public void setNewsDAO(INewsDAO newsDAO) {
		this.newsDAO = newsDAO;
	}

}
