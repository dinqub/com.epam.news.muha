package com.epam.muha.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.epam.muha.entity.Author;
import com.epam.muha.entity.News;
import com.epam.muha.entity.NewsVO;
import com.epam.muha.entity.Tag;
import com.epam.muha.exception.ServiceException;
import com.epam.muha.service.IAuthorService;
import com.epam.muha.service.ICommentService;
import com.epam.muha.service.INewsManagerService;
import com.epam.muha.service.INewsService;
import com.epam.muha.service.ITagService;

/**
 * The Class MainLogic.
 */
//@Service("MainService")
@Transactional
public class NewsManagerService implements INewsManagerService {

	/** The author logic. */
	//@Autowired
	//@Qualifier("AuthorService")
	public IAuthorService authorService ;

	/** The comment logic. */
	//@Autowired
	//@Qualifier("CommentService")
	public ICommentService commentService;

	/** The tag logic. */
	//@Autowired
	//@Qualifier("TagService")
	public ITagService tagService;

	/** The news logic. */
	//@Autowired
	//@Qualifier("NewsService")
	public INewsService newsService;

	public List<NewsVO> searchNewsByAuthor(long authorId)
			throws ServiceException {
		List<NewsVO> newsVO = new ArrayList<NewsVO>();
		List<News> newses = newsService.searchByAuthor(authorId);

		for (News news : newses) {
			NewsVO temp = new NewsVO();
			temp.setNews(news);
			temp.setAuthors(authorService.getAuthorByNewsId(news.getId()));
			temp.setTags(tagService.getTagsByNews(news.getId()));
			temp.setComments(commentService.getAll(news.getId()));
			newsVO.add(temp);
		}
		return newsVO;
	}

	public List<NewsVO> searchNewsByTags(List<Tag> tags)
			throws ServiceException {
		List<NewsVO> newsVO = new ArrayList<NewsVO>();
		List<News> newses = newsService.searchByTags(tags);

		for (News news : newses) {
			NewsVO temp = new NewsVO();
			temp.setNews(news);
			temp.setAuthors(authorService.getAuthorByNewsId(news.getId()));
			temp.setTags(tagService.getTagsByNews(news.getId()));
			temp.setComments(commentService.getAll(news.getId()));
			newsVO.add(temp);
		}
		return newsVO;
	}

	public void addNews(NewsVO newsVO) throws ServiceException {
		long id = newsService.add(newsVO.getNews());
		authorService.add(newsVO.getAuthors(), id);
		List<Tag> tags = newsVO.getTags();
		for (Tag tag : tags) {
			tagService.add(tag.getId(), id);
		}
	}

	public void removeNews(long id) throws ServiceException {
		commentService.removeByNewsId(id);
		newsService.remove(id);
		//If remove cascade
		List<Tag> tags=tagService.getTagsByNews(id);
		for (Tag tag : tags) {
			tagService.remove(id, tag.getId());
		}
		Author author=authorService.getAuthorByNewsId(id);
		authorService.remove(author.getId(), id);
	}

	public NewsVO getNews(long id) throws ServiceException {
		NewsVO newsVO = new NewsVO();
		News news = newsService.get(id);
		newsVO.setNews(news);
		newsVO.setAuthors(authorService.getAuthorByNewsId(news.getId()));
		newsVO.setTags(tagService.getTagsByNews(news.getId()));
		newsVO.setComments(commentService.getAll(news.getId()));
		return newsVO;
	}

	public void updateNews(NewsVO newsVO)
			throws ServiceException {
		News news=newsVO.getNews();
		newsService.update(news);
		authorService.updateNewsAuthor(newsVO.getAuthors(), news.getId());
		tagService.updateAllTagByNews(newsVO.getNews().getId(), newsVO.getTags());

	}

	public List<NewsVO> getSortedNews() throws ServiceException {
		List<NewsVO> newsVO = new ArrayList<NewsVO>();
		List<News> newses = newsService.getAllNews();

		for (News news : newses) {
			NewsVO temp = new NewsVO();
			temp.setNews(news);
			temp.setAuthors(authorService.getAuthorByNewsId(news.getId()));
			temp.setTags(tagService.getTagsByNews(news.getId()));
			temp.setComments(commentService.getAll(news.getId()));
			newsVO.add(temp);
		}
		return newsVO;
	}

	public void setLogic(ITagService tag, ICommentService comment, INewsService news,
			IAuthorService author) {
		this.authorService = author;
		this.commentService = comment;
		this.newsService = news;
		this.tagService = tag;
	}

	public void setAuthorService(IAuthorService authorService) {
		this.authorService = authorService;
	}

	public void setCommentService(ICommentService commentService) {
		this.commentService = commentService;
	}

	public void setTagService(ITagService tagService) {
		this.tagService = tagService;
	}

	public void setNewsService(INewsService newsService) {
		this.newsService = newsService;
	}

}
