package com.epam.muha.service;

import java.util.List;

import com.epam.muha.entity.NewsVO;
import com.epam.muha.entity.Tag;
import com.epam.muha.exception.ServiceException;

/**
 * The Interface IMainLogic.
 */
public interface INewsManagerService {

	/**
	 * Search news by author id.
	 *
	 * @param authorId - id of author works of than we want found
	 * @return the list of news writing by author
	 * @throws ServiceException the technical logic exception
	 */
	public List<NewsVO> searchNewsByAuthor(long authorId) throws ServiceException;

	/**
	 * Search news by list of tags.
	 *
	 * @param tags- list of tags in which we will be search
	 * @return the list of news which consist any tags
	 * @throws ServiceException the technical logic exception
	 */
	public List<NewsVO> searchNewsByTags(List<Tag> tags) throws ServiceException ;
	
	
	/**
	 * Add the news.
	 *
	 * @param newsVO the news shell
	 * @throws ServiceException the technical logic exception
	 */
	public void addNews(NewsVO newsVO) throws ServiceException ;

	/**
	 * Remove news by news id
	 *
	 * @param id - id of news
	 * @throws ServiceException the technical logic exception
	 */
	public void removeNews(long id) throws ServiceException ;

	/**
	 * Get the news by id
	 *
	 * @param id - id of news
	 * @return the news by id
	 * @throws ServiceException the technical logic exception
	 */
	public NewsVO getNews(long id)throws ServiceException ;

	/**
	 * Update news by id
	 *
	 * @param newsVO - new news
	 * @throws ServiceException the technical logic exception
	 */
	public void updateNews(NewsVO newsVO) throws ServiceException;

	/**
	 * Get the sorted list of news.
	 *
	 * @return the sorted list of news
	 * @throws ServiceException the technical logic exception
	 */
	public List<NewsVO> getSortedNews() throws ServiceException ;
	
	/**
	 * Sets the logic.
	 *
	 * @param tag - tag logic
	 * @param comment - comment logic
	 * @param news - news logic
	 * @param author - author logic
	 */
	public void setLogic(ITagService tag,ICommentService comment,INewsService news,IAuthorService author);
}

