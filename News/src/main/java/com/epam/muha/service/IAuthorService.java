package com.epam.muha.service;

import java.util.List;

import com.epam.muha.dao.IAuthorDAO;
import com.epam.muha.entity.Author;
import com.epam.muha.exception.DAOException;
import com.epam.muha.exception.ServiceException;


/**
 * The Interface IAuthorLogic.
 */
public interface IAuthorService {

	/**
	 * Add author
	 *
	 * @param author - author
	 * @throws ServiceException the technical logic exception
	 */
	public void add(Author author) throws ServiceException;
	
	/**
	 * Add authors news 
	 *
	 * @param author - author which write news
	 * @param newsID - id of news which author writing
	 * @throws ServiceException the technical logic exception
	 */
	public void add(Author author,long newsID) throws ServiceException;

	/**
	 * Removes author
	 *
	 * @param id - id of author
	 * @throws ServiceException the technical logic exception
	 */
	public void expired(long id) throws ServiceException;
	
	/**
	 * Remove author from news
	 * @param author - id of removed author
	 * @param newsId - id of news
	 * @throws DAOException
	 */
	public void remove(long authorid,long newsId) throws ServiceException;

	/**
	 * Gets author by id
	 *
	 * @param id - id of author
	 * @return author by id
	 * @throws ServiceException the technical logic exception
	 */
	public Author get(long id) throws ServiceException;
	
	/**
	 * Gets author by news id.
	 *
	 * @param newsId - id of news
	 * @return author by news id
	 * @throws ServiceException the technical logic exception
	 */
	public Author getAuthorByNewsId(long newsId) throws ServiceException;
	
	/**
	 * Update author
	 *
	 * @param author - new author
	 * @throws ServiceException the technical logic exception
	 */
	public void update(Author author) throws ServiceException;
	
	/**
	 * Update author of news in database.
	 * @param author - new author of news 
	 * @param newsId - current news
	 * @throws ServiceException
	 */
	public void updateNewsAuthor(Author author, long newsId) throws ServiceException;

   	/**
	    * Gets the all author
	    *
	    * @return list of all authors
	    * @throws ServiceException the technical logic exception
	    */
	   public List<Author> getAll() throws ServiceException;

	/**
	 * Sets the dao.
	 *
	 * @param IAuthorDAO the new dao
	 */
	public void setAuthorDAO(IAuthorDAO IAuthorDAO);

}
