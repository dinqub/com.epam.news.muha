package com.epam.muha.dao;

import java.util.List;

import javax.sql.DataSource;

import com.epam.muha.entity.Author;
import com.epam.muha.exception.DAOException;



public interface IAuthorDAO {
	
	/**
	 * Adds the new Author in database.
	 *
	 * @param author new author
	 * @throws DAOException the technical dao exception
	 */
	public void add(Author author) throws DAOException;
	
	/**
	 * Adds the new News of Author in database.
	 *
	 * @param author - author which will be add
	 * @param newsId - the news which write author
	 * @throws DAOException the technical dao exception
	 */
	public void add(Author author,long newsId) throws DAOException;
	
	/**
	 * Remove author from news
	 * @param author - id of removed author
	 * @param newsId - id of news
	 * @throws DAOException
	 */
	public void remove(long authorid,long newsId) throws DAOException;

	/**
	 * Fared author and record about this in database.
	 *
	 * @param id - id of author
	 * @throws DAOException the technical dao exception
	 */
	public void expired(long id) throws DAOException;

	/**
	 * Gets author by id from database.
	 *
	 * @param id - id of Author
	 * @return author - author of id
	 * @throws DAOException the technical dao exception
	 */
	public Author get(long id) throws DAOException;
	
	/**
	 * Gets the author from database by news id.
	 *
	 * @param newsId - news id
	 * @return author by news id
	 * @throws DAOException the technical dao exception
	 */
	public Author getAuthorByNewsId(long newsId) throws DAOException;
	
	/**
	 * Gets the all authors from database.
	 *
	 * @return list of all author
	 * @throws DAOException the technical dao exception
	 */
	public List<Author> getAll() throws DAOException;
	
	/**
	 * Update author in database.
	 *
	 * @param author - new author
	 * @throws DAOException the technical dao exception
	 */
	public void update(Author author) throws DAOException;

	/**
	 * Update author of news in database.
	 * @param author - new author of news 
	 * @param newsId - current news
	 * @throws DAOException
	 */
	public void updateNewsAuthor(Author author, long newsId) throws DAOException;
	
	/**
	 * Drop author from database
	 *
	 * @param i the i
	 * @throws DAOException the technical dao exception
	 */
	public void delete(long i) throws DAOException;
	
	public DataSource getDataSource();



	
}
