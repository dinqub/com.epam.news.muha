package com.epam.muha.dao;

import java.util.List;

import javax.sql.DataSource;

import com.epam.muha.entity.Tag;
import com.epam.muha.exception.DAOException;


public interface ITagDAO {

	/**
	 * Add tag to news in database.
	 *
	 * @param tagId - tag id
	 * @param id - id of news which will be added tag
	 * @throws DAOException the technical dao exception
	 */
	public void add(long tagId,long id) throws DAOException;
	
	/**
	 * Adds tag in database.
	 *
	 * @param tag - tag
	 * @throws DAOException the technical dao exception
	 */
	public void add(Tag tag)  throws DAOException ;

	/**
	 * Removes tag from database by tag id.
	 *
	 * @param id -id of tag
	 * @throws DAOException the technical dao exception
	 */
	public void remove(long id) throws DAOException;
	
	/**
	 * Removes tag from news from database by news id and tag id.
	 *
	 * @param newsId - id of news
	 * @param id - id of tag
	 * @throws DAOException the technical dao exception
	 */
	public void remove(long newsId,long id) throws DAOException;

	/**
	 * Gets tag from database by id of tag.
	 *
	 * @param id - id of tag
	 * @return tag by id
	 * @throws DAOException the technical dao exception
	 */
	public Tag get(long id) throws DAOException;
	
	/**
	 * Gets list of tag from database by id news.
	 *
	 * @param newsId - id of news
	 * @return list of tag by id of news
	 * @throws DAOException the technical dao exception
	 */
	public List<Tag> getTagsByNews(long newsId) throws DAOException;

	/**
	 * Update tag in database by id of tag.
	 *
	 * @param tag - updating tag
	 * @throws DAOException the technical dao exception
	 */
	public void update(Tag tag) throws DAOException;
	/**
	 * Update list of tag for news
	 * @param newsId - id of updating news
	 * @param tags - list of updating tags
	 */
	public void updateAllTagByNews(long newsId, List<Tag> tags) throws DAOException ;

	/**
	 * Gets the all tag from database.
	 *
	 * @return list of all tag from database
	 * @throws DAOException the technical dao exception
	 */
	public List<Tag> getAll() throws DAOException;
	
	
	public DataSource getDataSource();

}
