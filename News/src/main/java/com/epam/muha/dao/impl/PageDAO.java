package com.epam.muha.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.epam.muha.dao.IPageDAO;
import com.epam.muha.entity.Author;
import com.epam.muha.entity.News;
import com.epam.muha.entity.Tag;
import com.epam.muha.exception.DAOException;

//@Repository("PageDAO")
public class PageDAO implements IPageDAO {

	public static final String SQL_SELECT_NEWS_FIRST = "WITH filtertag as (SELECT news.news_id,short_text,full_text,title,creation_date,modification_date FROM (news LEFT JOIN news_tag ON news.news_id=news_tag.news_id) ";
	public static final String SQL_SELECT_NEWS_SECOND = "GROUP BY (news.NEWS_ID,short_text,full_text,title,creation_date,modification_date)),     filterauthor as(SELECT filtertag.news_id,short_text,full_text,title,creation_date,modification_date FROM (filtertag INNER JOIN news_author ON filtertag.news_id=news_author.news_id)";
    public static final String SQL_SELECT_NEWS_THRID = "GROUP BY (filtertag.NEWS_ID,short_text,full_text,title,creation_date,modification_date)),resultset as(SELECT filterauthor.news_id,short_text,full_text,title,filterauthor.creation_date,modification_date FROM filterauthor ORDER BY (SELECT -COUNT(comments.NEWS_ID) AS countid FROM COMMENTS WHERE comments.news_id=filterauthor.news_id),filterauthor.title DESC) SELECT news_id,short_text,full_text,title,creation_date,modification_date FROM (SELECT news_id,short_text,full_text,title,creation_date,modification_date,ROWNUM RNUM FROM resultset WHERE ROWNUM <=?) WHERE rnum > ?";
	
	//@Autowired	
    private DataSource dataSource;

	public List<News> getFilterList(List<Tag> tags, List<Author> authors,long totalsize,long page) throws DAOException {
		List<News> result = new ArrayList<News>();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;

		StringBuilder query = new StringBuilder(SQL_SELECT_NEWS_FIRST);

		int size = tags.size();

		if (size != 0) {
			query.append(" WHERE news_tag.tag_id IN (");

			for (int i = 0; i < size; ++i) {
				if (i < size - 1)
					query.append(tags.get(i).getId() + ",");
				else
					query.append(tags.get(i).getId());
			}
			query.append(")");
		}

		query.append(SQL_SELECT_NEWS_SECOND);

		size = authors.size();
		if (size != 0) {
			query.append(" WHERE news_author.author_id IN (");

			for (int i = 0; i < size; ++i) {
				if (i < size - 1)
					query.append(authors.get(i).getId() + ",");
				else
					query.append(authors.get(i).getId());
			}
			query.append(")");
		}
		query.append(SQL_SELECT_NEWS_THRID);

		try {
			connection = DAOUtils.getConnection(dataSource);
			statement = connection.prepareStatement(query.toString());
			statement.setLong(1,totalsize*page);
			statement.setLong(2,totalsize*(page-1));
			
			
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				News news = new News();
				news.setId(resultSet.getLong("news_id"));
				news.setShortText(resultSet.getString("short_text"));
				news.setFullText(resultSet.getString("full_text"));
				news.setTitle(resultSet.getString("title"));
				news.setCreationDate(resultSet.getTimestamp("creation_date"));
				news.setModificationDate(resultSet.getDate("modification_date"));
				result.add(news);
			}

			
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtils.close(resultSet, statement, connection);
		}

		return result;
	}

	public News getFilterNews(List<Tag> tags, List<Author> authors, long id)  throws DAOException {
		News news=new News();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;

		StringBuilder query = new StringBuilder(SQL_SELECT_NEWS_FIRST);

		int size = tags.size();

		if (size != 0) {
			query.append(" WHERE news_tag.tag_id IN (");

			for (int i = 0; i < size; ++i) {
				if (i < size - 1)
					query.append(tags.get(i).getId() + ",");
				else
					query.append(tags.get(i).getId());
			}
			query.append(")");
		}

		query.append(SQL_SELECT_NEWS_SECOND);

		size = authors.size();
		if (size != 0) {
			query.append(" WHERE news_author.author_id IN (");

			for (int i = 0; i < size; ++i) {
				if (i < size - 1)
					query.append(authors.get(i).getId() + ",");
				else
					query.append(authors.get(i).getId());
			}
			query.append(")");
		}
		query.append(SQL_SELECT_NEWS_THRID);

		try {
			connection = DAOUtils.getConnection(dataSource);
			statement = connection.prepareStatement(query.toString());
			statement.setLong(1,id);
			statement.setLong(2,id-1);
			
			
			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				news.setId(resultSet.getLong("news_id"));
				news.setShortText(resultSet.getString("short_text"));
				news.setFullText(resultSet.getString("full_text"));
				news.setTitle(resultSet.getString("title"));
				news.setCreationDate(resultSet.getTimestamp("creation_date"));
				news.setModificationDate(resultSet.getDate("modification_date"));
			}

			
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtils.close(resultSet, statement, connection);
		}

		return news;
	}
	
	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

}
