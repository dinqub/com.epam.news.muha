package com.epam.muha.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.epam.muha.dao.ICommentDAO;
import com.epam.muha.entity.Comment;
import com.epam.muha.exception.DAOException;


/**
 * The Class CommentDAO.
 */
//@Repository("CommentDAO")
public class CommentDAO implements ICommentDAO {

	/** The Constant SQL_INSRERT_COMMENT. */
	public static final String SQL_INSERT_COMMENT = "INSERT INTO COMMENTS (comment_id,comment_text,creation_date,news_id) VALUES(COMMENT_seq.nextval,?,?,?)";
	
	/** The Constant SQL_DELET_COMMENT. */
	public static final String SQL_DELETE_COMMENT = "DELETE FROM COMMENTS WHERE COMMENT_id=(?)";
	
	/** The Constant SQL_SELECT_COMMENT. */
	public static final String SQL_SELECT_COMMENT = "SELECT COMMENT_id,COMMENT_TEXT,CREATION_DATE,news_id FROM COMMENTS WHERE COMMENT_id=(?)";
	
	/** The Constant SQL_SELECT_ALL_COMMENT. */
	public static final String SQL_SELECT_ALL_COMMENT = "SELECT COMMENT_id,COMMENT_TEXT,creation_date,news_id FROM COMMENTS WHERE news_id=?";
	
	/** The Constant SQL_UPDATE_COMMENT. */
	public static final String SQL_UPDATE_COMMENT = "UPDATE COMMENTS SET COMMENT_TEXT=?,CREATION_DATE=?,NEWS_ID=? WHERE COMMENT_id=?";
	
	/** The data source. */
	
	//@Autowired	
	private DataSource dataSource;
	
	
	public void add(Comment comment) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = DAOUtils.getConnection(dataSource);
			statement = connection.prepareStatement(SQL_INSERT_COMMENT);
			statement.setString(1, comment.getCommentText());
			statement.setTimestamp(2, new Timestamp(comment.getCreationDate().getTime()));
			statement.setLong(3, comment.getNewsId());

			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally {
			DAOUtils.close(null, statement, connection);
		}
	}


	public void remove(long id) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = DAOUtils.getConnection(dataSource);
			statement = connection.prepareStatement(SQL_DELETE_COMMENT);
			statement.setLong(1, id);

			statement.executeUpdate();
		} catch (SQLException e) {			
			throw new DAOException(e);			
		}finally {
			DAOUtils.close(null, statement, connection);
		}

	}


	public Comment get(long id) throws DAOException {
		Comment comment = new Comment();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet=null;
		try {
			connection = DAOUtils.getConnection(dataSource);
			statement = connection.prepareStatement(SQL_SELECT_COMMENT);
			statement.setLong(1, id);

			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				comment.setId(resultSet.getLong("comment_id"));
				comment.setCommentText(resultSet.getString("comment_text"));
				comment.setCreationDate(resultSet.getTimestamp("creation_date"));
				comment.setNewsId(resultSet.getLong("news_id"));
			}
		} catch (SQLException e) {		
			throw new DAOException(e);
		}finally {
			DAOUtils.close(resultSet, statement, connection);
		}
		return comment;
	}


	public void update(Comment comment) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		long id=comment.getId();
		try {
			connection = DAOUtils.getConnection(dataSource);
			statement = connection.prepareStatement(SQL_UPDATE_COMMENT);
			statement.setString(1, comment.getCommentText());
			statement.setTimestamp(2, new Timestamp(comment.getCreationDate().getTime()));
			statement.setLong(3, comment.getNewsId());
			statement.setLong(4, id);

			statement.executeUpdate();
		} catch (SQLException e) {		
			throw new DAOException(e);
		}finally {
			DAOUtils.close(null, statement, connection);
		}

	}


	public List<Comment> getAll(long id) throws DAOException {
		List<Comment> comments = new ArrayList<Comment>();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet=null;
		try {
			connection = DAOUtils.getConnection(dataSource);
			statement = connection.prepareStatement(SQL_SELECT_ALL_COMMENT);
			statement.setLong(1, id);

			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				Comment comment = new Comment();
				comment.setId(resultSet.getLong("comment_id"));
				comment.setCommentText(resultSet.getString("comment_text"));
				comment.setCreationDate(resultSet.getTimestamp("creation_date"));
				comment.setNewsId(resultSet.getLong("news_id"));
				comments.add(comment);
			}
		} catch (SQLException e) {		
			throw new DAOException(e);			
		}finally {
			DAOUtils.close(resultSet, statement, connection);
		}
		return comments;
	}


	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}


	public DataSource getDataSource() {
		return dataSource;
	}

}
