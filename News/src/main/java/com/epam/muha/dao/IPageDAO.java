package com.epam.muha.dao;

import java.util.List;

import com.epam.muha.entity.Author;
import com.epam.muha.entity.News;
import com.epam.muha.entity.Tag;
import com.epam.muha.exception.DAOException;

/**
 * The Interface IPageDAO.
 */
public interface IPageDAO {

		
	/**
	 * Gets the filter list.
	 *
	 * @param tags - list of  tags
	 * @param authors - list of authors
	 * @param size - count news in one page
	 * @param page - wanted page
	 * @return the filter list of news on 1 page
	 * @throws DAOException the DAO exception
	 */
	public List<News> getFilterList(List<Tag> tags, List<Author> authors,long size,long page) throws DAOException;

	/**
	 * Gets the filter news.
	 *
		 * @param tags - list of  tags
	 * @param authors - list of authors
	 * @param id the id
	 * @return the filter list of news on 1 page
	 * @throws DAOException the DAO exception
	 */
	public News getFilterNews(List<Tag> tags, List<Author> authors,long id) throws DAOException;
}
