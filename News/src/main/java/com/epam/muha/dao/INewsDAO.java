package com.epam.muha.dao;

import java.util.List;

import javax.sql.DataSource;

import com.epam.muha.entity.News;
import com.epam.muha.entity.Tag;
import com.epam.muha.exception.DAOException;

public interface INewsDAO {

	/**
	 * Adds new News in database.
	 *
	 * @param news - which will be add
	 * @return id of added news
	 * @throws DAOException the technical dao exception
	 */
	public long add(News news) throws DAOException;

	/**
	 * Removes news from database by news id.
	 *
	 * @param id - id of news
	 * @throws DAOException the technical dao exception
	 */
	public void remove(long id) throws DAOException;

	/**
	 * Gets news from database by id.
	 *
	 * @param id - id of news
	 * @return the news by id
	 * @throws DAOException the technical dao exception
	 */
	public News get(long id) throws DAOException ;
	
	/**
	 * Update news in database by id.
	 *
	 * @param news - updating news
	 * @throws DAOException the technical dao exception
	 */
	public void update(News news) throws DAOException;
		
	/**
	 * Gets the all news from database.
	 *
	 * @return list of all news from database
	 * @throws DAOException the technical dao exception
	 */
	public List<News> getAllNews() throws DAOException;

	/**
	 * Search news in database by author.
	 *
	 * @param authorId - author id by which we will by search
	 * @return the list of news which writing this author
	 * @throws DAOException the technical dao exception
	 */
	public List<News> searchByAuthor(long authorId) throws DAOException;

	/**
	 * Search news in database by tags.
	 *
	 * @param tags - list of tag which will be in news
	 * @return the list of news which consist one of more of required tags
	 * @throws DAOException the technical dao exception
	 */
	public List<News> searchByTags(List<Tag> tags) throws DAOException;

	public DataSource getDataSource();
	
}
