package com.epam.muha.dao.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.datasource.DataSourceUtils;

/**
 * The Class UtilitDAO.
 */
public class DAOUtils {

	private static Logger logger = Logger.getLogger(DAOUtils.class.getName());

	public static Connection getConnection(DataSource dataSource) {
		
		Connection connection = DataSourceUtils.getConnection(dataSource);
		
		return connection;

	}

	/**
	 * Close ResultSet ,Statement and Connection
	 *
	 * @param resultSet
	 *            - resultSet
	 * @param statment
	 *            - statement
	 * @param connection
	 *            - connection
	 */
	public static void close(ResultSet resultSet, Statement statment,
			Connection connection) {

		try {
			if (resultSet != null) {
				resultSet.close();
			}
		} catch (SQLException e) {
			logger.error("SQL exception (request or table failed): " + e);
		} finally {
			try {
				if (statment != null) {
					statment.close();
				}
			} catch (SQLException e) {
				logger.error("SQL exception (request or table failed): " + e);
			} finally {
				@SuppressWarnings("resource")
				ClassPathXmlApplicationContext cx = new ClassPathXmlApplicationContext(
						"springproperties.xml");
				DataSource dataSource = (DataSource) cx.getBean("dataSource");
				DataSourceUtils.releaseConnection(connection, dataSource);
			}
		}
	}

}
