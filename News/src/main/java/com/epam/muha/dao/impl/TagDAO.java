package com.epam.muha.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.transaction.annotation.Transactional;

import com.epam.muha.dao.ITagDAO;
import com.epam.muha.entity.Tag;
import com.epam.muha.exception.DAOException;

/**
 * The Class TagDAO.
 */
// @Repository("TagDAO")
public class TagDAO implements ITagDAO {

	/** The Constant SQL_INSRERT_TAG. */
	public static final String SQL_INSERT_TAG = "INSERT INTO tag (tag_id,tag_name) VALUES(tag_seq.nextval, ?)";

	/** The Constant SQL_INSRERT_NEWS_TAG. */
	public static final String SQL_INSERT_NEWS_TAG = "INSERT INTO news_tag (news_tag_id,tag_id,news_id) VALUES(news_tag_seq.nextval,?,?)";

	/** The Constant SQL_DELET_TAG. */
	public static final String SQL_DELETE_TAG = "DELETE FROM tag WHERE tag_id=?";

	public static final String SQL_DELETE_NEWS_LIST_TAG = "DELETE FROM news_tag WHERE news_id=?";

	/** The Constant SQL_DELET_TAG_BY_NEWS. */
	public static final String SQL_DELETE_TAG_BY_NEWS = "DELETE FROM news_tag WHERE (tag_id=(?) AND news_id=?)";
	
	public static final String SQL_DELETE_NEWS_BY_TAG = "DELETE FROM news_tag WHERE tag_id=? ";

	/** The Constant SQL_SELECT_TAG. */
	public static final String SQL_SELECT_TAG = "SELECT tag_id,tag_name FROM tag WHERE tag_id=?";

	/** The Constant SQL_SELECT_ALL_TAG. */
	public static final String SQL_SELECT_ALL_TAG = "SELECT tag_id,tag_name FROM tag";

	/** The Constant SQL_SELECT_NEWS_TAG. */
	public static final String SQL_SELECT_NEWS_TAG = "SELECT tag.tag_id,tag.tag_name FROM (tag INNER JOIN news_tag ON tag.tag_id=news_tag.tag_id)  WHERE news_tag.news_id=(?)";

	/** The Constant SQL_UPDATE_TAG. */
	public static final String SQL_UPDATE_TAG = "UPDATE tag SET tag_name=? WHERE tag_id=?";

	/** The data source. */
	// private static DataSource dataSource;

	// @Autowired
	private DataSource dataSource;

	
	public void add(Tag tag) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet=null;
		try {
			connection = DAOUtils.getConnection(dataSource);
			String generatedColumns[] = { "TAG_ID" };
			statement = connection.prepareStatement(SQL_INSERT_TAG,
					generatedColumns);
			statement.setString(1, tag.getName());

			statement.executeUpdate();

			resultSet = statement.getGeneratedKeys();
			if (resultSet.next()) {
				tag.setId(resultSet.getLong(1));
			} else {
				throw new SQLException("Creating tag failed, no ID obtained.");
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally {
			DAOUtils.close(resultSet, statement, connection);
		}
	}

	public void add(long tagId, long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = DAOUtils.getConnection(dataSource);
			statement = connection.prepareStatement(SQL_INSERT_NEWS_TAG);
			statement.setLong(1, tagId);
			statement.setLong(2, newsId);
			
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally {
			DAOUtils.close(null, statement, connection);
		}
	}

	@SuppressWarnings("resource")
	public void remove(long id) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;

		try {
			connection = DAOUtils.getConnection(dataSource);
			
			statement = connection.prepareStatement(SQL_DELETE_NEWS_BY_TAG);
			statement.setLong(1, id);

			statement.executeUpdate();
			
			statement = connection.prepareStatement(SQL_DELETE_TAG);
			statement.setLong(1, id);

			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally {
			DAOUtils.close(null, statement, connection);
		}
	}

	public void remove(long newsId, long id) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = DAOUtils.getConnection(dataSource);
			statement = connection.prepareStatement(SQL_DELETE_TAG_BY_NEWS);
			statement.setLong(1, id);
			statement.setLong(2, newsId);

			statement.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException(e);
		}finally {
			DAOUtils.close(null, statement, connection);
		}

	}

	public Tag get(long id) throws DAOException {
		Tag tag = new Tag();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet =null;
		try {
			connection = DAOUtils.getConnection(dataSource);
			statement = connection.prepareStatement(SQL_SELECT_TAG);
			statement.setLong(1, id);

			resultSet = statement.executeQuery();

			if (resultSet.next()) {
				tag.setId(resultSet.getLong("tag_id"));
				tag.setName(resultSet.getString("tag_name"));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally {
			DAOUtils.close(resultSet, statement, connection);
		}
		return tag;

	}

	public List<Tag> getTagsByNews(long newsId) throws DAOException {
		List<Tag> tags = new ArrayList<Tag>();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			connection = DAOUtils.getConnection(dataSource);
			statement = connection.prepareStatement(SQL_SELECT_NEWS_TAG);
			statement.setLong(1, newsId);

			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				Tag tag = new Tag();
				tag.setId(resultSet.getLong("tag_id"));
				tag.setName(resultSet.getString("tag_name"));
				tags.add(tag);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally {
			DAOUtils.close(resultSet, statement, connection);
		}
		return tags;
	}

	public void update( Tag tag) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		long id=tag.getId();
		try {
			connection = DAOUtils.getConnection(dataSource);
			statement = connection.prepareStatement(SQL_UPDATE_TAG);
			statement.setString(1, tag.getName());
			statement.setLong(2, id);

			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally {
			DAOUtils.close(null, statement, connection);
		}

	}

	public void updateAllTagByNews(long newsId,List<Tag> tags)throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = DAOUtils.getConnection(dataSource);
			statement = connection.prepareStatement(SQL_DELETE_NEWS_LIST_TAG);
			statement.setLong(1, newsId);

			statement.executeUpdate();
			for (Tag tag : tags) {
				statement = connection.prepareStatement(SQL_INSERT_NEWS_TAG);
				statement.setLong(1, tag.getId());
				statement.setLong(2, newsId);
				statement.executeUpdate();
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally {
			DAOUtils.close(null, statement, connection);
		}

	}

	public List<Tag> getAll() throws DAOException {
		List<Tag> tag = new ArrayList<Tag>();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			connection = DAOUtils.getConnection(dataSource);
			statement = connection.prepareStatement(SQL_SELECT_ALL_TAG);

			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				Tag temp = new Tag();
				temp.setId(resultSet.getLong("tag_id"));
				temp.setName(resultSet.getString("tag_name"));
				tag.add(temp);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally {
			DAOUtils.close(resultSet, statement, connection);
		}
		return tag;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public DataSource getDataSource() {
		return dataSource;
	}

}
