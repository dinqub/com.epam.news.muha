package com.epam.muha.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import com.epam.muha.dao.IAuthorDAO;
import com.epam.muha.entity.Author;
import com.epam.muha.exception.DAOException;

/**
 * The Class AuthorDAO.
 */
// @Repository("AuthorDAO")
public class AuthorDAO implements IAuthorDAO {

	/** The Constant SQL_INSRERT_AUTHOR. */
	public static final String SQL_INSERT_AUTHOR = "INSERT INTO author (author_id,author_name) VALUES(author_seq.nextval, ?)";

	/** The Constant SQL_INSRERT_NEWS_AUTHOR. */
	public static final String SQL_INSERT_NEWS_AUTHOR = "INSERT INTO news_author (news_author_id,author_id,news_id) VALUES(news_author_seq.nextval,?,?)";

	/** The Constant SQL_UPDATE_NEWS_AUTHOR. */
	public static final String SQL_UPDATE_NEWS_AUTHOR = "UPDATE news_author SET author_id=? WHERE news_id=?";

	/** The Constant SQL_DROP_AUTHOR. */
	public static final String SQL_DELETE_AUTHOR = "DELETE FROM author WHERE author.author_id=(?)";
	
	public static final String SQL_DELETE_AUTHOR_BY_NEWS = "DELETE FROM news_author WHERE (author_id=(?) AND news_id=?)";
	
	public static final String SQL_DELETE_NEWS_BY_AUTHOR = "DELETE FROM news_author WHERE author_id=? ";

	/** The Constant SQL_DELET_AUTHOR. */
	public static final String SQL_EXPIRED_AUTHOR = "UPDATE author SET expired=? WHERE author_id=?";

	/** The Constant SQL_SELECT_AUTHOR. */
	public static final String SQL_SELECT_AUTHOR = "SELECT author_id,author_name,expired FROM author WHERE author_id=(?)";

	/** The Constant SQL_SELECT_ALL_AUTHOR. */
	public static final String SQL_SELECT_ALL_AUTHOR = "SELECT author_id,author_name,expired FROM author";

	/** The Constant SQL_UPDATE_AUTHOR. */
	public static final String SQL_UPDATE_AUTHOR = "UPDATE author SET author_name=? WHERE author_id=?";

	/** The Constant SQL_SELECT_NEWS_AUTHOR. */
	public static final String SQL_SELECT_NEWS_AUTHOR = "SELECT author.author_id,author.author_name,author.expired FROM (author INNER JOIN news_author ON author.author_id=news_author.author_id)  WHERE news_author.news_id=(?)";

	/** The data source. */
	// @Autowired
	private DataSource dataSource;


	public void add(Author author) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = DAOUtils.getConnection(dataSource);
			statement = connection.prepareStatement(SQL_INSERT_AUTHOR);
			statement.setString(1, author.getName());
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtils.close(null, statement, connection);
		}

	}

	public void add(Author author, long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = DAOUtils.getConnection(dataSource);
			statement = connection.prepareStatement(SQL_INSERT_NEWS_AUTHOR);
			statement.setLong(1, author.getId());
			statement.setLong(2, newsId);
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtils.close(null, statement, connection);
		}
	}

	public void updateNewsAuthor(Author author, long newsId)
			throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = DAOUtils.getConnection(dataSource);
			statement = connection.prepareStatement(SQL_UPDATE_NEWS_AUTHOR);
			statement.setLong(1, author.getId());
			statement.setLong(2, newsId);
			statement.executeUpdate();			
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally {
			DAOUtils.close(null, statement, connection);
		}
	}

	public void expired(long id) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;

		try {
			connection = DAOUtils.getConnection(dataSource);
			statement = connection.prepareStatement(SQL_EXPIRED_AUTHOR);

			Date today = new Date();
			Timestamp date = new Timestamp(today.getTime());
			
			statement.setTimestamp(1, date);
			statement.setLong(2, id);

			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally {
			DAOUtils.close(null, statement, connection);
		}
	}
	
	public void remove(long authorid, long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = DAOUtils.getConnection(dataSource);
			statement = connection.prepareStatement(SQL_DELETE_AUTHOR_BY_NEWS);
			statement.setLong(1, authorid);
			statement.setLong(2, newsId);

			statement.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException(e);
		}finally {
			DAOUtils.close(null, statement, connection);
		}
		
	}

	/**
	 * Drop author from database
	 * 
	 * @param id
	 *            - id of author
	 * @throws DAOException
	 *             the technical dao exception
	 */
	@SuppressWarnings("resource")
	public void delete(long id) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = DAOUtils.getConnection(dataSource);
			statement = connection.prepareStatement(SQL_DELETE_NEWS_BY_AUTHOR);
			statement.setLong(1, id);	
			
			statement.executeUpdate();	
			
			statement = connection.prepareStatement(SQL_DELETE_AUTHOR);
			statement.setLong(1, id);		
			
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally {
			DAOUtils.close(null, statement, connection);
		}
	}

	public void update(Author author) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		long id=author.getId();
		try {
			connection = DAOUtils.getConnection(dataSource);
			statement = connection.prepareStatement(SQL_UPDATE_AUTHOR);
			statement.setString(1, author.getName());
			statement.setLong(2, id);

			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally {
			DAOUtils.close(null, statement, connection);
		}
	}

	public Author get(long id) throws DAOException {
		Author author = new Author();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet=null;
		try {
			connection = DAOUtils.getConnection(dataSource);
			statement = connection.prepareStatement(SQL_SELECT_AUTHOR);
			statement.setLong(1, id);

			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				author.setId(resultSet.getLong("author_id"));
				author.setName(resultSet.getString("author_name"));
				author.setExpired(resultSet.getTimestamp("expired"));
			} else {
				author = null;
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally {
			DAOUtils.close(resultSet, statement, connection);
		}
		return author;
	}

	public Author getAuthorByNewsId(long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		Author author = new Author();
		ResultSet resultSet=null;
		try {
			connection = DAOUtils.getConnection(dataSource);
			statement = connection.prepareStatement(SQL_SELECT_NEWS_AUTHOR);
			statement.setLong(1, newsId);

			resultSet = statement.executeQuery();
			if (resultSet.next()) {

				author.setId(resultSet.getLong("author_id"));
				author.setName(resultSet.getString("author_name"));
				author.setExpired(resultSet.getTimestamp("expired"));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally {
			DAOUtils.close(resultSet, statement, connection);
		}
		return author;

	}

	public List<Author> getAll() throws DAOException {
		List<Author> author = new ArrayList<Author>();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet=null;
		try {
			connection = DAOUtils.getConnection(dataSource);
			statement = connection.prepareStatement(SQL_SELECT_ALL_AUTHOR);

			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				Author temp = new Author();
				temp.setId(resultSet.getLong("author_id"));
				temp.setName(resultSet.getString("author_name"));
				temp.setExpired(resultSet.getTimestamp("expired"));
				author.add(temp);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally {
			DAOUtils.close(resultSet, statement, connection);
		}
		return author;
	}
	/**
	 * Sets the data source.
	 *
	 * @param data
	 *            the new data source
	 */

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public DataSource getDataSource() {
		return dataSource;
	}

}
