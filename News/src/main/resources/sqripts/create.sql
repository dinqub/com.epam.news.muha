--------------------------------------------------------
--  File created - Tuesday-April-16-2015   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Sequence AUTHOR_SEQ
--------------------------------------------------------
   CREATE SEQUENCE  "ADMIN"."AUTHOR_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 ;
--------------------------------------------------------
--  DDL for Sequence COMMENT_SEQ
--------------------------------------------------------
   CREATE SEQUENCE  "ADMIN"."COMMENT_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 ;
--------------------------------------------------------
--  DDL for Sequence NEWS_AUTHOR_SEQ
--------------------------------------------------------
   CREATE SEQUENCE  "ADMIN"."NEWS_AUTHOR_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 ;
--------------------------------------------------------
--  DDL for Sequence NEWS_SEQ
--------------------------------------------------------
   CREATE SEQUENCE "ADMIN"."NEWS_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 ;
--------------------------------------------------------
--  DDL for Sequence NEWS_TAG_SEQ
--------------------------------------------------------
   CREATE SEQUENCE  "ADMIN"."NEWS_TAG_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 ;
--------------------------------------------------------
--  DDL for Sequence TAG_SEQ
--------------------------------------------------------
   CREATE SEQUENCE  "ADMIN"."TAG_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 ;
--------------------------------------------------------
--  DDL for Table AUTHOR
--------------------------------------------------------
  CREATE TABLE "ADMIN"."AUTHOR" 
   (	"AUTHOR_ID" NUMBER(20,0) NOT NULL , 
	"AUTHOR_NAME" NVARCHAR2(30) NOT NULL , 
	"EXPIRED" TIMESTAMP (6),
	CONSTRAINT "AUTHOR_PK" PRIMARY KEY ("AUTHOR_ID")
   ) ;
--------------------------------------------------------
--  DDL for Table COMMENTS
--------------------------------------------------------

  CREATE TABLE "ADMIN"."COMMENTS" 
   (	"COMMENT_ID" NUMBER(20,0) NOT NULL , 
	"COMMENT_TEXT" NVARCHAR2(20) NOT NULL , 
	"CREATION_DATE" TIMESTAMP (6) NOT NULL , 
	"NEWS_ID" NUMBER NOT NULL ,
	CONSTRAINT "COMMENTS_PK" PRIMARY KEY ("COMMENT_ID"),
	 CONSTRAINT "COMMENTS_FK1" FOREIGN KEY ("NEWS_ID")
	  REFERENCES  "ADMIN"."NEWS" ("NEWS_ID") 
   );
--------------------------------------------------------
--  DDL for Table NEWS
--------------------------------------------------------

  CREATE TABLE "ADMIN"."NEWS" 
   (	"NEWS_ID" NUMBER(20,0) NOT NULL , 
	"SHORT_TEXT" NVARCHAR2(20) NOT NULL , 
	"FULL_TEXT" NVARCHAR2(20) NOT NULL , 
	"TITLE" NVARCHAR2(20) NOT NULL , 
	"CREATION_DATE" TIMESTAMP (6) NOT NULL , 
	"MODIFICATION_DATE" DATE NOT NULL ,
	CONSTRAINT "NEWS_PK" PRIMARY KEY ("NEWS_ID")
   ) ;
------------------------
--  DDL for Table NEWS_AUTHOR
--------------------------------------------------------

  CREATE TABLE "ADMIN"."NEWS_AUTHOR" 
   (	"NEWS_AUTHOR_ID" NUMBER(20,0) NOT NULL , 
	"NEWS_ID" NUMBER(20,0) NOT NULL , 
	"AUTHOR_ID" NUMBER(20,0) NOT NULL ,
	CONSTRAINT "NEWS_AUTOR_PK" PRIMARY KEY ("NEWS_AUTHOR_ID"),
	CONSTRAINT "NEWS_AUTHOR_UK1" UNIQUE ("NEWS_ID"),
	 CONSTRAINT "NEWS_AUTHOR_FK1" FOREIGN KEY ("AUTHOR_ID")
	  REFERENCES  "ADMIN"."AUTHOR" ("AUTHOR_ID")  , 
	 CONSTRAINT "NEWS_AUTHOR_FK2" FOREIGN KEY ("NEWS_ID")
	  REFERENCES  "ADMIN"."NEWS" ("NEWS_ID") 
   ) ;
--------------------------------------------------------
--  DDL for Table NEWS_TAG
--------------------------------------------------------

  CREATE TABLE "ADMIN"."NEWS_TAG" 
   (	"NEWS_TAG_ID" NUMBER(20,0) NOT NULL , 
	"NEWS_ID" NUMBER(20,0) NOT NULL , 
	"TAG_ID" NUMBER(20,0) NOT NULL ,
	CONSTRAINT "NEWS_TAG_PK" PRIMARY KEY ("NEWS_TAG_ID"),
	 CONSTRAINT "NEWS_TAG_FK1" FOREIGN KEY ("NEWS_ID")
	  REFERENCES  "ADMIN"."NEWS" ("NEWS_ID"), 
	 CONSTRAINT "NEWS_TAG_FK2" FOREIGN KEY ("TAG_ID")
	  REFERENCES  "ADMIN"."TAG" ("TAG_ID") 
   );
--------------------------------------------------------
--  DDL for Table ROLES
--------------------------------------------------------

  CREATE TABLE "ADMIN"."ROLES" 
   ("USER_ID" NUMBER(20,0) NOT NULL , 
	"ROLE_NAME" VARCHAR2(50 BYTE) NOT NULL ,
	CONSTRAINT "ROLES_PK" PRIMARY KEY ("USER_ID")
   );
--------------------------------------------------------
--  DDL for Table TAG
--------------------------------------------------------

  CREATE TABLE "ADMIN"."TAG" 
   ("TAG_ID" NUMBER(20,0) NOT NULL , 
	"TAG_NAME" NVARCHAR2(20) NOT NULL ,
	CONSTRAINT "TAG_PK" PRIMARY KEY ("TAG_ID")
   );
--------------------------------------------------------
--  DDL for Table USER_TABLE
--------------------------------------------------------

  CREATE TABLE "ADMIN"."USER_TABLE" 
   (	"USER_ID" NUMBER(20,0) NOT NULL , 
	"USRE_NAME" NVARCHAR2(20) NOT NULL , 
	"LOGIN" VARCHAR2(30 BYTE) NOT NULL , 
	"PASSWORD" VARCHAR2(30 BYTE) NOT NULL ,
	CONSTRAINT "USER_TABLE_PK" PRIMARY KEY ("USER_ID")
   );
